Exchange module.

Requirements:

Windows:
 - OpenSSL
Linux:
 - python3.7
 - python3.7-dev
 - git
 - openssl
 - libssl-dev
 - python3-setuptools
 - build-essential

Installing requirements:
  # For Linux
    1. Install
      sudo apt-get install python3.7 python3.7-dev git openssl libssl-dev python3-setuptools build-essential

  # For Windows
    1. Download and install OpenSSL-Win32
    2. If you see error: not found libeay32.lib download and copy to OpenSSL-Win32\lib