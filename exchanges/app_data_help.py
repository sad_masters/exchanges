from pathlib import Path

_project_name = "exchanges"
AppData = Path.home() / f".{_project_name}"
currencies = {"BTC": "Bitcoin",
              "USD": "US Dollar",
              "USDT": "Tether",
              "ETH": "Etherium",
              "XMR": "Monero"}
_files = {AppData / "cash.json": "{}", AppData / "currencies.json": '{"BTC": "Bitcoin"}'}

_msg = f"Application data directory %s is %s."


def create_application_data_folder():
    if AppData.exists():
        print(_msg % (AppData, "exists"))
    else:
        AppData.mkdir()
        print(_msg % (AppData, "created"))

    for file in _files:
        if file.exists():
            print(_msg % (file, "exists"))
        else:
            file.touch()
            file.write_text('{}')
            print(_msg % (file, "created"))

    test_keybase = Path.cwd() / "data_source" / "test.kdbx"

    from shutil import copy
    copy(str(test_keybase), str(AppData / "test.kdbx"))


def reset_application_data_folder():
    for file in map(lambda path: path.is_exists, _files):
        file.write_text("{}")


if __name__ == '__main__':
    from fire import Fire

    Fire({
        "create": create_application_data_folder,
        "reset": reset_application_data_folder
    })
