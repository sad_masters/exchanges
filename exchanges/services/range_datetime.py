from datetime import datetime, timedelta
from typing import Union, Tuple


def range_datetime(start: datetime, end: Union[datetime, timedelta], step: timedelta) -> Tuple[datetime]:

    if isinstance(end, timedelta):
        end = start + end

    current = start

    while current <= end:
        yield current
        current += step

    yield end
