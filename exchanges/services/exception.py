class ExchangeException(Exception):
    pass


class ExchangeNotSupport(ExchangeException):
    pass


class EmptyResult(ExchangeException):
    pass


class ExchangeNotSupportAtDt(ExchangeNotSupport):
    pass
