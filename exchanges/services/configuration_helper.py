from dataclasses import fields
from pathlib import Path
from runpy import run_path


def _check_path(path: Path):

    if not path.exists():
        raise ValueError(f"Can't find configuration_tools file by path {str(path)}")

    if path.suffix != '.py':
        raise TypeError(f"Can't parse configuration_tools file. file suffix != .py")


def from_file(*configure_structs, configuration_file: Path):
    _check_path(configuration_file)

    configure_structs = {conf.__name__: conf for conf in configure_structs}

    for name, configuration in filter(lambda obj: obj[0] in configure_structs.keys(), run_path(configuration_file).items()):

        _fields = fields(configure_structs[name])
        for field in _fields:
            setattr(configure_structs[name], 'field', getattr(configuration, field, _fields[field]))

    return configure_structs
