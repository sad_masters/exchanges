from copy import copy
from dataclasses import dataclass
from datetime import datetime, timedelta
from decimal import Decimal
from typing import Optional


def _parse_decimal(value):
    if isinstance(value, Decimal):
        return value

    if isinstance(value, str):
        return Decimal(value)

    if isinstance(value, float):
        return Decimal(str(value))  # to avoid error

    if isinstance(value, int):
        return Decimal.from_float(value)

    raise ValueError(f"Can't parse Decimal from {value} ({type(value)})")


@dataclass(repr=True)
class Candle:
    close: Decimal
    high: Decimal
    low: Decimal
    open: Decimal
    date_time: datetime
    volume: Optional[Decimal] = None
    base_volume: Optional[Decimal] = None

    weighted_average: Optional[Decimal] = None
    real_close_time: datetime = None
    interval: timedelta = None
    number_of_trades: int = None

    @property
    def close_time(self):
        return self.date_time + self.interval

    def __post_init__(self):
        self.close = _parse_decimal(self.close)
        self.open = _parse_decimal(self.open)
        self.low = _parse_decimal(self.low)
        self.high = _parse_decimal(self.high)

    def get_swap(self) -> 'Candle':
        candle = copy(self)

        candle.close = 1 / candle.close
        candle.open = 1 / candle.open
        candle.low = 1 / candle.low
        candle.high = 1 / candle.high
        candle.volume = None

        return candle

    def __add__(self, right: 'Candle'):
        left = copy(self)

        left.close = right.close
        left.high = max(left.high, right.high)
        left.low = min(left.low, right.low)
        left.volume += right.volume

        return left
