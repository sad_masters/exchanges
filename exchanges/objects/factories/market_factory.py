from typing import Union

from ..currency import Currency
from ..market import Market


class MarketFactory:
    @classmethod
    def from_bittrex(cls, market_bittrex):
        return Market(Currency.soft_get(market_bittrex['BaseCurrency']),
                      Currency.soft_get(market_bittrex["MarketCurrency"]),
                      '-')

    @classmethod
    def from_binance(cls, market_binance: dict):

        raw_market: str = market_binance['symbol']
        base_currency = ("BNB", "BTC", "ETH", "USDT", "TUSD", "USDC", "XRP", "PAX")

        def _parse(raw: str) -> Union[Currency, str]:
            try:
                return Currency.get(raw)

            except ValueError as exp:
                if raw in base_currency:
                    return raw

                raise exp

        try:
            sold, sale = Currency.get(raw_market[:3]), Currency.get(raw_market[3:])

        except ValueError:

            for base in filter(lambda currency: currency in raw_market, base_currency):
                index = raw_market.index(base)

                # Fix in future: core currency must parsing without soft_get
                try:
                    sold, sale = Currency.soft_get(raw_market[:index]), _parse(raw_market[index:])
                    break

                except ValueError:
                    continue
            else:
                raise ValueError("Can't parse market with {}".format(raw_market))

        return Market(sold, sale, '')

    @classmethod
    def from_idex(cls, market_idex):
        return Market(Currency.soft_get(market_idex[:market_idex.index('_')]),
                      Currency.soft_get(market_idex[market_idex.index('_') + 1:]), '_')

    @classmethod
    def from_poloniex(cls, market_poloniex):
        return Market(Currency.soft_get(market_poloniex[:market_poloniex.index('_')]),
                      Currency.soft_get(market_poloniex[market_poloniex.index('_') + 1:]), '_').get_swap()
