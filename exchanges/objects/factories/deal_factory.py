from datetime import datetime

from decimal import Decimal

from ..market import Market
from ..deal import Deal
from ..currency import Currency


class DealFactory:

    @classmethod
    def from_bittrex(cls, market: Market, raw: dict) -> Deal:
        try:
            timestamp = datetime.strptime(raw['TimeStamp'], "%Y-%m-%dT%H:%M:%S.%f")
        except ValueError:
            timestamp = datetime.strptime(raw['TimeStamp'], "%Y-%m-%dT%H:%M:%S")
        # All trades has 0,25% comission, but i don't know in USDT or market.sold

        return Deal(uid=raw['Id'],
                    date_time=timestamp,
                    quantity=Decimal(raw['Quantity']),
                    total=Decimal(raw['Total']),
                    fill_type=raw['FillType'],
                    order_type=raw['OrderType'],
                    market=market,
                    fee=(market.sold, Decimal(raw['Total']) * Decimal("0.0025")))

    @classmethod
    def from_binance(cls, market: Market, raw: dict) -> Deal:
        return Deal(uid=raw['id'],
                    date_time=raw['time'],
                    quantity=Decimal(raw['qty']),
                    total=Decimal(raw['price']) * Decimal(raw['qty']),
                    order_type='buy' if raw['isBuyer'] else 'sell',
                    fill_type=None,  # raw['isMaker'] ???
                    market=market,
                    fee=(Currency(raw['commissionAsset']), Decimal(raw['commission'])))

    @classmethod
    def from_idex(cls, market: Market, raw: dict) -> Deal:
        is_buy = raw['type'] == 'buy'

        return Deal(uid=raw['uuid'],
                    date_time=datetime.strptime(raw['date'], "%Y-%m-%d %H:%M:%S"),
                    quantity=Decimal(raw['amount']),
                    total=Decimal(raw['total']),
                    order_type=raw['type'],
                    fill_type=None,
                    market=market,
                    fee=(Decimal(raw['buyerFee' if is_buy else 'sellerFee']), market.sale if is_buy else market.sold),
                    price=Decimal(raw['price']))

    @classmethod
    def from_poloniex(cls, market: Market, raw: dict) -> Deal:
        return Deal(uid=raw['globalTradeID'],
                    date_time=datetime.strptime(raw['date'], "%Y-%m-%d %H:%M:%S"),
                    quantity=Decimal(raw['amount']),
                    total=Decimal(raw['total']),
                    order_type=raw['type'],
                    fill_type=None,  # raw['category']???
                    market=market,
                    fee=(market.sold, Decimal(raw['fee'])))
