from datetime import datetime, timedelta
from decimal import Decimal
from functools import reduce
from typing import Tuple, Dict

from ..candle import Candle


class CandleFactory:
    @staticmethod
    def from_bittrex(raw: dict, interval: timedelta) -> Candle:
        """
            Bittrex API 2 returning result with keys:
            BV: base_volume
            C: close
            H: high
            L: low
            O: open
            T: date_time
            V: volume24h
        """

        keys = {'BV': "base_volume",
                'C': 'close',
                'H': 'high',
                'O': 'open',
                'T': "date_time",
                'V': 'volume',
                'L': 'low'}

        kwargs = {keys[key]: value for key, value in raw.items()}

        kwargs['base_volume'] = Decimal(kwargs['base_volume'])

        kwargs['date_time'] = datetime.strptime(kwargs['date_time'], "%Y-%m-%dT%H:%M:%S")
        kwargs['volume'] = Decimal(kwargs['volume'])

        return Candle(interval=interval, **kwargs)

    @staticmethod
    def from_poloniex(raw: dict, interval: timedelta):
        """
         Api example

            'date': 1534680000,
            'high': 6457.77699999,
            'low': 6405,
            'open': 6428.35036649,
            'close': 6457.77699982,
            'volume': 158867.98771476,
            'quoteVolume': 24.71539742,
            'weightedAverage': 6427.89533241}

        :param interval:
        :param raw:
        :return:
        """

        raw['date_time'] = datetime.fromtimestamp(raw.pop('date'))
        raw['base_volume'] = raw.pop('quoteVolume')
        raw['weighted_average'] = raw.pop('weightedAverage')

        return Candle(interval=interval, **raw)

    @staticmethod
    def from_binance(raw: tuple, interval: timedelta):
        # [
        #     1499040000000,  # Open time
        #     "0.01634790",  # Open
        #     "0.80000000",  # High
        #     "0.01575800",  # Low
        #     "0.01577100",  # Close
        #     "148976.11427815",  # Volume
        #     1499644799999,  # Close time
        #     "2434.19055334",  # Quote asset volume
        #     308,  # Number of trades
        #     "1756.87402397",  # Taker buy base asset volume
        #     "28.46694368",  # Taker buy quote asset volume
        #     "17928899.62484339"  # Can be ignored
        # ]
        open_time, open_price, high, low, close, volume, close_time, quote_asset_volume, number_of_trades, _, _, _ = raw
        open_time = datetime.fromtimestamp(open_time / 1000)
        close_time = datetime.fromtimestamp(close_time / 1000)

        return Candle(interval=interval,
                      date_time=open_time,
                      base_volume=volume,
                      open=open_price,
                      high=high,
                      close=close,
                      real_close_time=close_time,
                      low=low,
                      volume=volume,
                      number_of_trades=number_of_trades)

    @classmethod
    def from_idex(cls, trades: Dict[datetime, Tuple[Decimal, Decimal]], start: datetime, interval: timedelta):
        """
        :param interval:
        :param trades:
            {
            datetime: (price, amount), ...
            }
        :return:
        """
        if len(trades) == 0:
            raise ValueError("Can't initialize candle from empty trade dict.")

        volume = sum(trade[1] for trade in trades.values())
        min_price = min(trade[0] for trade in trades.values())
        max_price = max(trade[0] for trade in trades.values())
        open_price = trades[min(trades.keys())][0]
        close_price = trades[max(trades.keys())][0]

        weighted_average = reduce(lambda weight_sum, p_a: weight_sum + p_a[0] * p_a[1], trades.values(),
                                  Decimal('0')) / volume
        return Candle(interval=interval,
                      date_time=start,
                      base_volume=None,
                      open=open_price,
                      close=close_price,
                      high=max_price,
                      low=min_price,
                      volume=volume,
                      weighted_average=weighted_average,
                      number_of_trades=len(trades),
                      real_close_time=max(trades.keys()))

    @classmethod
    def get_solo_candle(cls, price: Decimal, date_time: datetime, interval: timedelta):
        return Candle(interval=interval,
                      date_time=date_time,
                      volume=Decimal('0'),
                      open=price,
                      close=price,
                      high=price,
                      low=price,
                      number_of_trades=0,
                      base_volume=None)
