from ..market import Market
from ..order import Order


class OrderFactory:
    @classmethod
    def from_bittrex(cls, market: Market, order: dict, is_buy: bool):
        return Order(market, order['Rate'], order['Quantity'], is_buy=is_buy)

    @classmethod
    def from_binance(cls, market: Market, order: list, is_buy: bool):
        return Order(market, order[0], order[1], is_buy=is_buy)

    @classmethod
    def from_idex(cls, market: Market, order: dict, is_buy: bool):
        return Order(market, order['price'], order['amount'], is_buy=is_buy)

    @classmethod
    def from_poloniex(cls, market: Market, order: tuple, is_buy: bool):
        return Order(market.get_swap(), order[0], order[1], is_buy=is_buy)
