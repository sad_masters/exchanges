import logging
from enum import Enum

from ..storage.file_storage import FileStorage
from ..storage.main_storage import get_test_configuration


def find_in_enum(cls, item: str):
    for obj in cls:
        if item in (obj.name, obj.value):
            return obj

    raise ValueError("Can't find currency with ticker or name - %s" % item)


def soft_find_in_enum(cls, item: str):
    try:
        return find_in_enum(cls, item)

    except ValueError as e:
        logging.warning(str(e))
        return item


def eq(self, other):
    if not isinstance(other, str):
        other = other.ticker

    return self.ticker == other


def contains(cls, item: str) -> bool:
    try:
        find_in_enum(cls, item)
        return True
    except ValueError:
        return False


def updated_currency():
    try:
        currencies = get_test_configuration()[0].get_currencies()
    except ValueError:
        currencies = FileStorage.get_currencies_local_storage().get_currencies()

    currency = Enum(value='Currency', names=currencies)

    setattr(currency, "ticker", property(lambda self: self.name))
    setattr(currency, "title", property(lambda self: self.value))

    setattr(currency, "get", classmethod(find_in_enum))
    setattr(currency, "soft_get", classmethod(soft_find_in_enum))

    setattr(currency, "contains", classmethod(contains))

    setattr(currency, "__eq__", eq)

    return currency


Currency = updated_currency()
