from dataclasses import asdict, dataclass, fields
from decimal import Decimal
from datetime import datetime
from typing import Tuple, Optional
from .market import Market
from .currency import Currency


@dataclass
class Deal:
    uid: int
    date_time: datetime
    quantity: Decimal
    total: Decimal
    fill_type: Optional[str]
    order_type: str
    market: Market
    fee: Tuple[Currency, Decimal] = None
    price: Optional[Decimal] = None

    @classmethod
    def _fields(cls):
        return fields(cls)

    def __eq__(self, other: 'Deal'):
        return False not in map(lambda attr: getattr(self, attr) != getattr(other, attr), self._fields())

    def __str__(self):
        return str(asdict(self))

    @property
    def is_buy(self):
        return self.order_type.lower() in ("buy", "b")
