from enum import Enum

from typing import Union


class Platforms(Enum):
    Bittrex = 0
    Binance = 1
    Poloniex = 2
    Idex = 3

    @classmethod
    def get(cls, provider) -> 'Platforms':
        from exchanges.providers.service.exchange_interface import ExchangeInterface

        if isinstance(provider, ExchangeInterface):
            provider = provider.__class__

        for pl in cls:
            if pl.provider == provider:
                return pl

        raise ValueError(f"Can't find platform by {provider}")

    @classmethod
    def soft_get(cls, provider) -> Union['Platforms', str]:
        try:
            return cls.get(provider)
        except ValueError:
            return str(provider)

    @classmethod
    def get_name(cls, provider):
        instance = cls.get(provider)

        return "Unknown" if instance is None else instance.name

    @property
    def provider(self):
        if self is Platforms.Bittrex:
            from exchanges.providers import BittrexProvider as Provider

        elif self is Platforms.Binance:
            from exchanges.providers import BinanceProvider as Provider

        elif self is Platforms.Poloniex:
            from exchanges.providers import PoloniexProvider as Provider

        elif self is Platforms.Idex:
            from exchanges.providers import IdexProvider as Provider

        else:
            raise ValueError(f"Can't find provider of this platform {self}")

        return Provider
