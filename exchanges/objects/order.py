from decimal import Decimal

from .market import Market


class Order:
    def __init__(self, market: Market, price: Decimal, quantity: Decimal, is_buy: bool):
        self.market = market
        self.price = price
        self.quantity = quantity
        self.is_buy = is_buy

    def __eq__(self, other: 'Order'):
        return self.market == other.market and \
               self.price == other.price and \
               self.quantity == other.quantity and \
               self.is_buy == other.is_buy

    def __str__(self):
        return f"{str(self.market)}: " \
               f"type: {'buy' if self.is_buy else 'sell'} " \
               f"price:{self.price}, " \
               f"quantity:{self.quantity}"
