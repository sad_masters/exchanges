from decimal import Decimal
from typing import Tuple, List, Callable, Iterator

from exchanges import Order, Currency
from exchanges.objects.market import Market

import pandas as pd


class UserError(Exception):
    def __init__(self, error_text: str="unexpected error"):
        self.msg = error_text


class Glass:
    def __init__(self, order_getter: Callable[[Currency, Currency], Tuple[Order]], market: Market):
        self._order_getter = order_getter

        self.market = market
        self._orders: List[Order] = list()

        self.curr_price: Decimal = None
        self.update_orders()

    def __len__(self):
        return self._orders.__len__()

    def __str__(self):
        sell = ','.join(map(str, self.sell_orders))
        buy = ','.join(map(str, self.buy_orders))

        return f'{sell};{buy}'

    def update_orders(self):
        self._orders = list(self._order_getter(self.market.sold, self.market.sale))
        self._orders.sort(key=lambda order: order.price)

    @property
    def middle_price(self) -> Decimal:

        sell = Decimal(next(self.sell_orders).price)
        buy = Decimal(next(filter(lambda order: order.is_buy, self._orders[::-1])).price)

        return (sell + buy) / Decimal('2')

    @property
    def sell_orders(self) -> Iterator[Order]:
        return filter(lambda order: not order.is_buy, self._orders)

    @property
    def buy_orders(self) -> Iterator[Order]:
        return filter(lambda order: order.is_buy, self._orders)

    @staticmethod
    def get_data_frame(orders: List) -> pd.DataFrame:
        orders_frame = pd.DataFrame(
            {"price": [Decimal(order.price) for order in orders],
             "quantity": [Decimal(order.quantity) for order in orders],
             "is buy": [order.is_buy for order in orders]})
        return orders_frame
