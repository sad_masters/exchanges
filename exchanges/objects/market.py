from dataclasses import dataclass, field

from .currency import Currency


@dataclass
class Market:
    base: Currency
    market: Currency
    delimiter: str = field(default=" ")

    def __str__(self):
        return "{}{}{}".format(getattr(self.base, "ticker", self.base),
                               self.delimiter,
                               getattr(self.market, "ticker", self.market))

    def __hash__(self):
        return self.__str__().__hash__()

    def __eq__(self, other: 'Market'):
        return self.base == other.base and \
               self.market == other.market and \
               self.delimiter == other.delimiter

    def __getitem__(self, item):
        if item is 0:
            return self.sold
        if item is 1:
            return self.sale

        raise ValueError()

    def get_swap(self):
        return Market(self.market, self.base, self.delimiter)

    @property
    def sold(self) -> Currency:
        return self.base

    @property
    def sale(self) -> Currency:
        return self.market

    @property
    def currencies(self) -> tuple:
        return self.sold, self.sale

    @property
    def is_pseudo(self):
        return self.base is self.market

    def __contains__(self, item: Currency):
        return item == self.base or item == self.market

    def get_normal(self):
        return Market(self.base, self.market, " ")
