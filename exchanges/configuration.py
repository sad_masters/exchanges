from collections import namedtuple

from .app_data_help import AppData

_server = "0.0.0.0"


class Mongo:
    Host = _server
    Port = 27017
    DatabaseName = "exchanges"
    User = ''
    Password = ''


class AppDataStorage:
    Cash = AppData / "cash.json"
    Currencies = AppData / "currencies.json"


class KeePass:
    Path = AppData / "test.kdbx"
    MasterPassword = "test1"
    KeyFile = None
    DefaultGroup = "General"


# for title, res in from_file(Mongo, KeePass, configuration_file="configuration"):
#     setattr(sys.modules[__name__], title, res)

Assemble = namedtuple('Assemble', ("Keys", 'KeysHandler', "Cash", "CashHandlers"))

Test = Assemble(KeePass, 'KeePassStorage', AppDataStorage, 'FileStorage')
Release = Assemble(KeePass, 'KeePassStorage', Mongo, 'MongoStorage')
