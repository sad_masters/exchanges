import unittest
import json
from datetime import datetime
from decimal import Decimal
from functools import reduce

from nose.tools import assert_equal, assert_raises
from pytz import utc

from exchanges.objects.platforms import Platforms
from simple_manager import SimpleManager as Manager
from exchanges.objects.currency import Currency
from exchanges.services import ExchangeNotSupport


def added_test_currencies():
    from data_source.test_data import currencies_path
    with open(currencies_path, 'r') as file:
        for ticker in filter(lambda t: not Currency.contains(t), json.load(file)):
            setattr(Currency, ticker, ticker)


class ManagerTests(unittest.TestCase):
    def __init__(self, methodName='runTest'):
        self.dt = datetime(2017, 11, 21, 16, 30, 8)
        self.dt_end = datetime(2018, 8, 20, 16, 30, 8)

        self.historical_dt = datetime(2018, 8, 23, 1, 38, 0)
        self.dt_now_utc = datetime.utcnow().replace(tzinfo=utc)

        self.now = datetime.now()
        self.manager = Manager(Platforms.Poloniex, Platforms.Idex, Platforms.Binance, Platforms.Bittrex)

        added_test_currencies()
        super().__init__(methodName)

    def test_get_price(self):
        price = self.manager.get_price(Currency.ETH, Currency.ETH, self.historical_dt)
        assert_equal(Decimal('1'), price, msg="ExchangeManager get_price")

        price = self.manager.get_price(Currency.ETH, Currency.OMG, self.historical_dt)
        assert_equal(Decimal('75.53440592189742427675806330'), price, msg="ExchangeManager get_price")

    def test_get_markets(self):
        markets = self.manager.get_markets()
        providers = self.manager._detached_providers
        result_markets = tuple(reduce(lambda res, provider: res.union(provider.get_markets()), providers, set()))

        assert_equal(markets, result_markets, msg='ExchangeManager get_markets')

    def test_get_deposit_history(self):
        deposit_history = self.manager.get_deposit_history(Currency.ETH, self.dt, self.dt_end)
        result_deposit_history = list()
        result_deposit_history.append(('0xe06c11e5ed607f74585e68b9a50299e1be8ff238', '8.86332413', 1524821329,
                                       'COMPLETE',
                                       '0xe99757478efa82ab4ebed549fa4f0e944e53608de6b086e74e2154255211e035'))
        assert_equal(tuple(result_deposit_history), deposit_history, msg='ExchangeManager get_deposit_history')

    def test_get_withdraw_history(self):
        withdraw_history = self.manager.get_withdraw_history(Currency.ETH, self.dt, self.dt_end)
        result_withdraw_history = list()
        result_withdraw_history.append(('0x9d84ECEd4eE167A424E2BEb12fFAd0D6Da2AcF4b', '0.51000000', 1526237888,
                                        'COMPLETE: 0xef608ce4c71e5678afeaefa8c752e124a932a0266258bbdfd878a513c4dd1f91',
                                        None))
        assert_equal(tuple(result_withdraw_history), withdraw_history, msg='ExchangeManager get_withdraw_history')

    def test_get_trading_deals(self):
        trading_deals = self.manager.get_trading_deals(Currency.ETH)
        assert_equal(trading_deals, tuple())

    def test_get_order_book(self):
        with assert_raises(ExchangeNotSupport):
            self.manager.get_order_book(Currency.ETH, Currency.OMG)

    def test_get_open_orders(self):
        open_orders = self.manager.get_my_open_orders(Currency.OMG)
        assert_equal(open_orders, tuple(), msg='ExchangeManager get_open_orders')

    def test_get_deposit_address(self):
        deposit_address = self.manager.get_deposit_address(Currency.ETH)

        result_deposit_address = {Platforms.Poloniex: '0xe06c11e5ed607f74585e68b9a50299e1be8ff238',
                                  Platforms.Idex: '0x37ed755bef274d0c52907710a084b51ea66f632f44e07c2e9500cb34d3905843',
                                  Platforms.Binance: '0x84fc3279d1d108b9867ccbe67cf71391ec5af25c'}

        for platform in deposit_address.keys():
            assert_equal(result_deposit_address[platform], deposit_address[platform],
                         msg=f'Error in {platform.name} eth deposit address')

    def _test_get_all_deposit_addresses(self):

        from data_source.test_data import deposit_addresses_path

        with open(deposit_addresses_path, 'r') as file:
            result_deposit_addresses: dict = json.load(fp=file)

        for platform, keys in self.manager.get_all_deposit_addresses().items():
            platform_keys = result_deposit_addresses.pop(platform.name)

            for currency, expected_address in keys.items():
                address = platform_keys.pop(getattr(currency, 'value', currency))
                assert_equal(address, expected_address, msg=f"Error in {platform} for {currency}")

        assert_equal(len(result_deposit_addresses), 0, msg="Not all keys")

    def test_get_indirect_price(self):
        indirect_price = self.manager.get_indirect_price(Currency.XRP, Currency.DASH, datetime(2018, 8, 1, 16, 30, 8))
        assert_equal(Decimal("0.002020751828542269093383228440"),
                     indirect_price,
                     msg="MonoManagerTest get_indirect_price with date ")

        price = self.manager.get_indirect_price(Currency.ETH, Currency.OMG, self.historical_dt)
        assert_equal(Decimal('75.53440592189742427675806330'), price, msg="ExchangeManager get_price with date")
