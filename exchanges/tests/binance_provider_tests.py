import hashlib
import hmac
import logging
import unittest
from datetime import datetime, timedelta
from decimal import Decimal
from pathlib import Path

import requests
from nose.tools import assert_equal, assert_raises, assert_in

from ..objects import OrderFactory
from ..objects.currency import Currency
from ..objects.market import Market
from ..providers.binance_provider import BinanceProvider
from ..services.exception import ExchangeNotSupport
from ..storage.file_storage import FileStorage


class BinanceProviderTests(unittest.TestCase):

    def __init__(self, methodName='runTest'):

        self.api_key = 'wiSLGgBoz1PxMZDubmvxyis0KFMjvY4RJ8itbrpQQeS5dp2cKsHbgGVfJBbHXPez'
        self.secret = 'mzjCVBXluXsvldZft7yzpw5t0SgntKtCnJ7oz15UUsYuf8ElqLWOc5BW6XzysFY3'

        fs = FileStorage.from_configuration()

        self.provider = BinanceProvider(key=self.api_key, secret=self.secret, cash_interface=fs)
        # 1528620780581 in milliseconds
        self.dt = datetime(2018, 7, 9, 11, 53, 00, 581000)
        self.date_time_end = datetime(2018, 7, 10, 11, 53, 00, 581000)

        self.ltc_currency = Currency.get("LTC")
        self.btc_currency = Currency.get("BTC")
        self.eth_currency = Currency.get("ETH")
        self.dash_currency = Currency.get("DASH")
        self.xrp_currency = Currency.get("XRP")
        self._markets = None

        super().__init__(methodName)
        self.maxDiff = None

    def get_request(self, command, params=None):
        session = requests.session()
        session.headers.update({'Accept': 'application/json',
                                'User-Agent': 'binance/python',
                                'X-MBX-APIKEY': self.api_key})
        timestmp = int(round(float(dict(requests.get('https://api.binance.com/api/v1/time').json()).get('serverTime'))))
        request_url = 'https://api.binance.com' + command + '?'
        params_url = 'timestamp=' + str(timestmp) if not params else 'timestamp=' + str(timestmp) + params
        hmac_new = hmac.new(key=bytearray(self.secret, encoding='utf-8'), msg=params_url.encode('utf-8'),
                            digestmod=hashlib.sha256)
        result = session.get(request_url + params_url + '&signature=' + hmac_new.hexdigest()).json()
        session.close()

        return result

    def test_get_price(self):
        '''for using now time use utc timezone time like in poloniex
        dt = int(datetime.now().timestamp() * 1000) # work
        agg_trade = list(requests.get('https://api.binance.com/api/v1/aggTrades',
                                      {'symbol': 'LTCBTC',
                                       'limit': '1'}).json())
        rate = Decimal(dict(agg_trade[len(agg_trade) - 1]).get('p')) if len(agg_trade) != 0 else None'''

        price = self.provider.get_price(self.ltc_currency, self.btc_currency, self.date_time_end)
        assert_equal(Decimal("0.01174700"), price, msg='Test get_price with all valid parameters')

        price = self.provider.get_price(self.ltc_currency, self.ltc_currency, self.date_time_end)
        assert_equal(Decimal("1"), price, msg="Test get_price when currencies are equals")

        with assert_raises(ExchangeNotSupport):
            self.provider.get_price(Currency.get("XRP"), Currency.get("DASH"))

    def test_get_deposit_history(self):
        deposit_history = self.provider.get_deposit_history(self.btc_currency, self.dt,
                                                            self.date_time_end)
        assert_equal(deposit_history, tuple(), msg='Binance get_deposit_history')

    def test_get_withdraw_history(self):
        withdraw_history = self.provider.get_withdraw_history(self.btc_currency, self.dt,
                                                              self.date_time_end)
        assert_equal(withdraw_history, tuple(), msg='Binance get_deposit_history')

    def test_get_order_book(self):
        # Comment: This test is need request! Without request return test_data already updated.
        order_book = self.provider.get_order_book(self.ltc_currency, self.btc_currency)

        order_book_req = dict(requests.get('https://api.binance.com/api/v1/depth',
                                           {'symbol': 'LTCBTC', 'limit': '1000'}).json())
        order_book_req.pop('lastUpdateId')
        market = Market(Currency.get('LTC'), Currency.get('BTC'), '')
        bids = map(lambda order: OrderFactory.from_binance(market, order, True), order_book_req.get('bids'))
        asks = map(lambda order: OrderFactory.from_binance(market, order, True), order_book_req.get('asks'))

        result_order_book = tuple(bids) + tuple(asks)

        # TODO: VERY LONG
        for order in order_book:
            assert_in(order, result_order_book, msg='Binance get_order_book')

    def test_get_open_orders(self):
        open_orders = self.provider.get_my_open_orders(self.ltc_currency)
        assert_equal(open_orders, tuple(), msg='Binance get_open_orders')

    def test_get_trading_deals(self):
        tarding_deals = self.provider.get_trading_deals(self.ltc_currency)
        assert_equal(tarding_deals, tuple(), msg='Binance get_trading_deals')

    def test_get_exchange_currencies(self):
        exchange_currencies = self.provider.get_currencies()
        result_exchange_currencies = (Currency.get('PPT'), Currency.get('CDT'), Currency.get('XZC'),
                                      Currency.get('NEO'), Currency.get('POWR'), Currency.get('CHAT'),
                                      Currency.get('CLOAK'), Currency.get('AE'), Currency.get('GVT'),
                                      Currency.get('SNT'), Currency.get('TUSD'), Currency.get('VET'),
                                      Currency.get('WAVES'), Currency.get('APPC'), Currency.get('XVG'),
                                      Currency.get('STEEM'), Currency.get('NANO'), Currency.get('LTC'),
                                      Currency.get('MDA'), Currency.get('BLZ'), Currency.get('WABI'),
                                      Currency.get('LSK'), Currency.get('AST'), Currency.get('XRP'),
                                      Currency.get('CND'), Currency.get('DNT'), Currency.get('ARDR'),
                                      Currency.get('LINK'), 'HC', Currency.get('ICN'), Currency.get('QSP'),
                                      Currency.get('QKC'), Currency.get('TNB'), Currency.get('IOST'),
                                      Currency.get("ELF"), Currency.get("POE"), Currency.get("TRIG"),
                                      Currency.get("ZIL"), Currency.get("ARK"), Currency.get("KMD"),
                                      Currency.get("NEBL"), Currency.get("QLC"), Currency.get("LRC"),
                                      Currency.get("BNT"), Currency.get("DOCK"), Currency.get("TNT"),
                                      Currency.get("REP"), Currency.get("DASH"), Currency.get("ZEN"),
                                      Currency.get("BTS"), "PHX", Currency.get("EOS"), Currency.get("ENG"),
                                      Currency.get("SNM"), Currency.get("BTG"), Currency.get("STRAT"),
                                      Currency.get("DATA"), Currency.get("REQ"), Currency.get("GXS"),
                                      Currency.get("RLC"), Currency.get("NPXS"), Currency.get("ZRX"),
                                      Currency.get("TRX"), Currency.get("FUN"), Currency.get("RCN"),
                                      Currency.get("WPR"), Currency.get("NPX"), Currency.get("ETC"),
                                      Currency.get("QTUM"), Currency.get("KEY"), Currency.get("MANA"),
                                      Currency.get("HOT"), Currency.get("PIVX"), Currency.get("XLM"),
                                      Currency.get("ICX"), Currency.get("XMR"), Currency.get("BCPT"),
                                      Currency.get("EVX"), Currency.get("VIB"), Currency.get("SKY"),
                                      Currency.get("ARN"), Currency.get("ZEC"), Currency.get("CVC"),
                                      Currency.get("GRS"), Currency.get("VIA"), Currency.get("MFT"),
                                      Currency.get("SALT"), Currency.get("LOOM"), Currency.get("BTC"),
                                      Currency.get("POLY"), Currency.get("STORM"), Currency.get("IOTA"),
                                      Currency.get("ETH"), Currency.get("AGI"), Currency.get("NAV"),
                                      Currency.get("EDO"), Currency.get("THETA"), Currency.get("NXS"),
                                      Currency.get("SNGLS"), Currency.get("WINGS"), Currency.get("NAS"),
                                      Currency.get("SYS"), Currency.get("WTC"), Currency.get("OST"),
                                      Currency.get("GTO"), Currency.get("MTH"), Currency.get("NCASH"),
                                      Currency.get("MTL"), Currency.get("BCC"), Currency.get("RDN"),
                                      Currency.get("WAN"), Currency.get("CMT"), Currency.get("ENJ"),
                                      Currency.get("GAS"), Currency.get("LEND"), Currency.get("SUB"),
                                      Currency.get("SETH"), Currency.get("DENT"), Currency.get("MOD"),
                                      Currency.get("POA"), Currency.get("BCN"), Currency.get("FUEL"),
                                      Currency.get("INS"), Currency.get("KNC"), Currency.get("STORJ"),
                                      Currency.get("BNB"), Currency.get("BQX"), Currency.get("ONT"),
                                      Currency.get("BRD"), Currency.get("XEM"), Currency.get("OAX"),
                                      Currency.get("IOTX"), Currency.get("ADX"), Currency.get("LUN"),
                                      Currency.get("YOYO"), Currency.get("AION"), Currency.get("DGD"),
                                      Currency.get("GNT"), Currency.get("DLT"), Currency.get("BCD"),
                                      Currency.get("USDT"), Currency.get("SC"), Currency.get("OMG"),
                                      Currency.get("VIBE"), Currency.get("ADA"), Currency.get("NULS"),
                                      Currency.get("BAT"), Currency.get("MCO"), Currency.get("AMB"),
                                      Currency.get("GO"))

        for currency in exchange_currencies:
            assert_in(currency, result_exchange_currencies, msg='Binance get_exchange_currencies')


    def test_get_balance(self):
        balance = self.provider.get_balance(self.btc_currency)
        assert_equal(balance, Decimal("0"), msg='Binance get_balance')

    def test_get_deposit_address(self):
        deposit_account = self.provider.get_deposit_address(self.btc_currency)
        assert_equal(deposit_account, "18LvXWz4wSoySnYXYAUSoBDvpxHJogTs7F", msg='Binance get_deposit_account')

    def test_get_all_deposit_addresses(self):
        result_deposit_addresses = dict()
        for balance in list(dict(self.get_request('/api/v3/account')).get('balances')):
            asset = dict(balance).get('asset')
            deposit_address = dict(self.get_request('/wapi/v3/depositAddress.html', '&asset=BTC'))

            if deposit_address:
                try:
                    currency = Currency.get(asset)
                    result_deposit_addresses[currency] = deposit_address.get('address')

                except ValueError as exception:
                    # Log it
                    logging.error(str(exception))
                    continue

        if not result_deposit_addresses:
            result_deposit_addresses = None

        if result_deposit_addresses is None:
            with assert_raises(ExchangeNotSupport):
                self.provider.get_all_deposit_addresses()
        else:
            deposit_addresses = self.provider.get_all_deposit_addresses()
            for deposit_address in deposit_addresses:
                assert_in(deposit_address, result_deposit_addresses, msg="Binance get_all_deposit_address")

    def test_get_indirect_price(self):
        price = self.provider.get_indirect_price(self.ltc_currency, self.btc_currency, self.date_time_end)
        assert_equal(Decimal("0.01174700"), price, msg='Test get_price with all valid parameters')

        price = self.provider.get_indirect_price(self.dash_currency, self.xrp_currency, self.dt)
        assert_equal(Decimal("505.2734648202545810602881523"), price, msg="Idex get_indirect_price")

    def _test_get_candles(self):
        for interval in self.provider.get_valid_intervals():
            candles = self.provider.get_candles(sold=Currency.BTC,
                                                sale=Currency.USDT,
                                                term=timedelta(minutes=59),
                                                interval=interval)
            print(candles)
