import hashlib
import hmac
import http
import http.client
import json
import time
import unittest
import urllib
from datetime import datetime, timedelta
from decimal import Decimal
from functools import reduce
from pathlib import Path
from urllib.parse import urlparse

import requests
from nose.tools import assert_equal, assert_raises, assert_in
from pytz import utc

from ..objects import OrderFactory, MarketFactory
from ..objects.currency import Currency
from ..objects.market import Market
from ..providers.poloniex_provider import PoloniexProvider
from ..services.exception import ExchangeNotSupport, EmptyResult
from ..storage.file_storage import FileStorage


class PoloniexProviderTests(unittest.TestCase):

    def __init__(self, methodName='runTest'):

        self.api_key = '8W6QXER6-4193KOK6-YTW4LFKX-OWGMQJSO'
        self.secret = 'e3a91370ceed8acd81fe8ff4233d800450142bd21b81c35aa888e190a0b44999a13d0b6da4009b94816e5f9d99d1b3' \
                      '110c197921a33b46714d40486d2f3fa616'
        fs = FileStorage(Path.cwd() / "data_source" / "cash_data" / "cash.json")
        self.poloniex_provider = PoloniexProvider(self.api_key, self.secret, fs)
        self.date_time = datetime(2017, 11, 21, 16, 30, 8)
        self.date_time_now = datetime.utcnow().replace(tzinfo=utc)
        self.usdt_currency = Currency.get("USDT")
        self.btc_currency = Currency.get("BTC")
        self.dash_currency = Currency.get("DASH")
        self.exmr_currency = Currency.get("EXMR")
        self.xrp_currency = Currency.get("XRP")
        self._markets = None

        super().__init__(methodName)

    def post_request(self, command, currencyPair=None, start=None, end=None):
        options = {'command': command} if not currencyPair else {'command': command, 'currencyPair': currencyPair}
        if start is not None and end is not None:
            options.update({'start': start, 'end': end})
        request_url = 'https://poloniex.com/tradingApi'
        http_method = 'POST'
        time.sleep(0.2)
        payload = {'nonce': int(round(time.time() * 1000))}
        if options:
            payload.update(options)
        payload = urllib.parse.urlencode(payload)
        hmac_new = hmac.new(key=bytearray(self.secret, encoding='utf-8'), digestmod=hashlib.sha512)
        hmac_new.update(payload.encode('utf-8'))
        sign = hmac_new.hexdigest()
        headers = {'Content-type': 'application/x-www-form-urlencoded',
                   'Key': self.api_key,
                   'Sign': sign}
        url_o = urlparse(request_url)
        conn = http.client.HTTPSConnection(url_o.netloc)
        conn.request(http_method, request_url, payload, headers)
        response = conn.getresponse().read()
        conn.close()
        return json.loads(response.decode('utf-8'))

    def test_get_price(self):
        # dt_time = int(self.date_time_now.timestamp())
        price = self.poloniex_provider.get_price(self.btc_currency, self.usdt_currency, self.date_time)
        assert_equal(price, Decimal("8242.58785000"), msg='Poloniex get_price')

        price = self.poloniex_provider.get_price(self.usdt_currency, self.usdt_currency, self.date_time)
        assert_equal(price, Decimal("1"), msg='Poloniex get_price')

        with assert_raises(ExchangeNotSupport):
            self.poloniex_provider.get_price(self.dash_currency, self.exmr_currency, self.date_time)

    def test_get_deposit_history(self):
        with assert_raises(ExchangeNotSupport):
            self.poloniex_provider.get_deposit_history(self.btc_currency,
                                                       self.date_time,
                                                       self.date_time_now)

    def test_get_withdraw_history(self):
        with assert_raises(ExchangeNotSupport):
            self.poloniex_provider.get_withdraw_history(self.btc_currency,
                                                        self.date_time,
                                                        self.date_time_now)

    def test_get_order_book(self):
        result = dict(requests.get('https://poloniex.com/public',
                                   {'command': 'returnOrderBook', 'currencyPair': 'BTC_ETH'}).json())
        if result:
            market = Market(Currency.get('BTC'), Currency.get('ETH'), '_')
            bids = map(lambda order: OrderFactory.from_poloniex(market, order, True), result.get('bids'))
            asks = map(lambda order: OrderFactory.from_poloniex(market, order, False), result.get('asks'))
            result_order_book = tuple(bids) + tuple(asks)
        else:
            result_order_book = None

        if result_order_book is None:
            with assert_raises(ExchangeNotSupport):
                self.poloniex_provider.get_order_book(self.btc_currency, self.usdt_currency)
        else:
            order_book = self.poloniex_provider.get_order_book(self.btc_currency, self.usdt_currency)

            # TODO: VERY LONG
            for order in result_order_book:
                assert_in(order, order_book, msg='Poloniex get_order_book')

    def test_get_open_orders(self):
        open_orders = self.poloniex_provider.get_my_open_orders(self.dash_currency)
        assert_equal(open_orders, tuple(), msg='Poloniex get_open_orders')

    def test_get_trading_deals(self):
        tarding_deals = self.poloniex_provider.get_trading_deals(self.dash_currency)
        assert_equal(tarding_deals, tuple(), msg='Poloniex get_trading_deals')

    def test_get_exchange_currencies(self):
        exchange_currencies = self.poloniex_provider.get_currencies()

        result = requests.get('https://poloniex.com/public',
                              {'command': 'returnTicker'}).json()

        exchange_currencies_req = tuple(
            reduce(lambda res, market: res.union(MarketFactory.from_poloniex(market).currencies),
                   list(dict(result).keys()),
                   set()))

        for currency in exchange_currencies:
            assert_in(currency, exchange_currencies_req, msg='Poloniex get_exchange_currencies')

    def test_get_balance(self):
        balance = self.poloniex_provider.get_balance(self.dash_currency)
        assert_equal(balance, Decimal("0"), msg='Poloniex get_balance')

    def test_get_deposit_address(self):
        with assert_raises(ExchangeNotSupport):
            self.poloniex_provider.get_deposit_address(self.btc_currency)

    def test_get_all_deposit_addresses(self):
        with assert_raises(EmptyResult):
            self.poloniex_provider.get_all_deposit_addresses()

    def test_get_indirect_price(self):
        price = self.poloniex_provider.get_indirect_price(self.dash_currency, self.xrp_currency, self.date_time)
        assert_equal(Decimal("2064.159200551343900758097864"), price, msg="Poloniex get_indirect_price")

        price = self.poloniex_provider.get_indirect_price(self.usdt_currency, self.btc_currency, self.date_time)
        assert_equal(price, Decimal("0.0001213211212544128358910969933"), msg='Poloniex get_price')

    def test_get_candles(self):
        for interval in self.poloniex_provider.get_valid_intervals():
            candles = self.poloniex_provider.get_candles(Currency.BTC, Currency.USDT, term=timedelta(days=1), interval=interval)
            print(candles)
