import logging
import unittest
from pprint import pprint

from app_data_folder_helper import create_application_data_folder
from exchanges.tests.binance_provider_tests import BinanceProviderTests
from exchanges.tests.bittrex_provider_tests import BittrexProviderTests
from exchanges.tests.idex_provider_tests import IdexProviderTests
from exchanges.tests.poloniex_provider_tests import PoloniexProviderTests
from tests.manager_tests import ManagerTests

if __name__ == '__main__':
    create_application_data_folder()

    test_suit = unittest.TestSuite()

    log_filename: str = "all_test.log"
    logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                        level=logging.ERROR,
                        filename=log_filename,
                        filemode='w')

    test_suit.addTest(unittest.makeSuite(BinanceProviderTests))
    test_suit.addTest(unittest.makeSuite(BittrexProviderTests))
    test_suit.addTest(unittest.makeSuite(IdexProviderTests))
    test_suit.addTest(unittest.makeSuite(PoloniexProviderTests))
    test_suit.addTest(unittest.makeSuite(ManagerTests))

    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(test_suit)

    with open(log_filename, 'r') as file:
        pprint(file.readlines())
