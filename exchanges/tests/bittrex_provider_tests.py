import hashlib
import hmac
import json
import logging
import time
import unittest
from dataclasses import asdict
from datetime import datetime
from decimal import Decimal
from functools import reduce
from importlib.resources import read_text
from pathlib import Path

import requests
from nose.tools import assert_equal, assert_raises, assert_in

import exchanges.data_source.test_data.bittrex as res
from ..objects import DealFactory, OrderFactory
from ..objects.currency import Currency
from ..objects.factories import MarketFactory
from ..objects.market import Market
from ..providers import BittrexProvider
from ..services.exception import ExchangeNotSupport
from ..storage.file_storage import FileStorage


class BittrexProviderTests(unittest.TestCase):

    def __init__(self, methodName='runTest'):
        logging.basicConfig(level="DEBUG")
        self.api_key = 'd65ecf064bbe4c70822b798f904f6a2c'
        self.secret = '8cf5a2a668ad4638b0b1590f13e12f10'
        fs = FileStorage(Path.cwd() / "data_source" / "cash_data" / "cash.json")
        self.provider = BittrexProvider(key=self.api_key, secret=self.secret, cash_interface=fs)
        self.date_time = datetime(2017, 11, 1, 16, 30, 8)
        self.ETH = Currency.get("ETH")
        self.btc_currency = Currency.get("BTC")
        self.DASH = Currency.get("DASH")
        self.xmr_currency = Currency.get("XMR")
        self._markets = None

        super().__init__(methodName)

    def get_request(self, command, params=None):
        nonce = str(int(time.time() * 1000))
        request_url = 'https://bittrex.com/api' + command \
                      + '?apikey=' + self.api_key \
                      + '&nonce=' + nonce \
                      + params if params else 'https://bittrex.com/api/v1.1/account/' + command \
                                              + '?apikey=' + self.api_key \
                                              + '&nonce=' + nonce
        apisign = hmac.new(self.secret.encode('utf-8'),
                           request_url.encode('utf-8'),
                           hashlib.sha512).hexdigest()
        return requests.get(request_url,
                            headers={"apisign": apisign},
                            timeout=10).json()

    def test_get_price(self):

        price = self.provider.get_price(self.DASH, self.DASH, self.date_time)
        assert_equal(Decimal("1"), price, msg="Test get_price when currencies are equals")

        with assert_raises(ExchangeNotSupport):
            self.provider.get_price(self.DASH, self.xmr_currency)

        price = self.provider.get_price(self.DASH, self.ETH, self.date_time)
        assert_equal(Decimal("0.91000000000000003108624468950438313186168670654296875"), price, msg="Test get_price with all valid parameters")

    def test_get_deposit_history(self):
        deposit_history = self.provider.get_deposit_history(self.btc_currency, self.date_time, datetime.now())
        assert_equal(deposit_history, tuple(), msg='Bittrex get_deposit_history')

    def test_get_withdraw_history(self):
        withdraw_history = self.provider.get_withdraw_history(self.btc_currency, self.date_time, datetime.now())
        assert_equal(withdraw_history, tuple(), msg='Bittrex get_deposit_history')

    def test_get_order_book(self):
        # Comment: need request in this test, because ~200 elements in order_book and they updates each time
        # without date parameter we are not track test_data
        order_book = self.provider.get_order_book(self.ETH, self.btc_currency)

        result = dict(dict(requests.get('https://bittrex.com/api/v1.1/public/getorderbook',
                                        {'market': 'BTC-ETH',
                                         'marketname': 'BTC-ETH',
                                         'type': 'both'}).json()).get('result'))
        market = Market(Currency.get('BTC'), Currency.get('ETH'), '-')
        buy = map(lambda order: OrderFactory.from_bittrex(market, order, True), result.get('buy'))
        sell = map(lambda order: OrderFactory.from_bittrex(market, order, False), result.get('sell'))
        result_order_book = tuple(buy) + tuple(sell)

        # TODO: VERY LONG
        for order in order_book:
            assert_in(order, result_order_book, msg='Bittrex get_order_book')

    def test_get_open_orders(self):
        open_orders = self.provider.get_my_open_orders(self.btc_currency)

        result_open_orders = []
        for market in dict(requests.get('https://bittrex.com/api/v1.1/public/getmarkets').json())['result']:
            market_name = dict(market).get('MarketName')
            if 'BTC' in market_name:
                result = self.get_request('/v1.1/market/getopenorders',
                                          '&market=' + market_name + '&marketname=' + market_name)
                if not result.get('result') or not dict(result.get('result')).get('result'):
                    continue
                for order in list(dict(result.get('result')).get('result')):
                    is_buy = False
                    if order['OrderType'] == "LIMIT_BUY":
                        is_buy = True
                    result_open_orders.append(OrderFactory.from_bittrex(
                        Market(Currency(market_name[:market_name.index('-')]),
                               Currency(market_name[market_name.index('-') + 1:]),
                               '-'), order, is_buy))

        for order in open_orders:
            assert_in(order, result_open_orders)

    def test_get_trading_deals(self):
        trarding_deals = self.provider.get_trading_deals(self.btc_currency)

        result_trade_deals = []
        for market in list(dict(requests.get('https://bittrex.com/api/v1.1/public/getmarkets').json()).get('result')):
            market_name = dict(market).get('MarketName')
            if 'BTC' in market_name:
                trades = dict(requests.get('https://bittrex.com/api/v1.1/public/getmarkethistory',
                                           {'market': market_name}).json()).get('result')
                if trades:
                    for trade in list(trades):
                        result_trade_deals.append(DealFactory.from_bittrex(MarketFactory.from_bittrex(market), trade))
        for deal in trarding_deals:
            assert_in(deal, result_trade_deals, msg='Bittrex get_trading_deals')

    def test_get_exchange_currencies(self):
        exchange_currencies = self.provider.get_currencies()
        exchange_currencies_req = tuple(reduce(lambda res, market: res.union(MarketFactory.from_bittrex(market).currencies), list(dict(requests.get('https://bittrex.com/api/v1.1/public/getmarkets').json()).get('result')), set()))
        # AssertionError: <Currency.BTG: 'BTG'> not found
        for currency in exchange_currencies:
            assert_in(currency, exchange_currencies_req, msg='Bittrex get_exchange_currencies')

    def test_get_balance(self):
        balance = self.provider.get_balance(self.btc_currency)
        assert_equal(balance, Decimal("0"), msg='Test get_balance')

    def test_get_deposit_address(self):
        # TODO: account not verified!!!
        result = self.get_request('/v1.1/account/getdepositaddress', '&currency=BTC&currencyname=BTC')
        result_deposit_account = dict(dict(result).get('result')).get('Address') if dict(result).get('result') else None

        if result_deposit_account is None:
            with assert_raises(ExchangeNotSupport):
                self.provider.get_deposit_address(self.btc_currency)
        else:
            deposit_account = self.provider.get_deposit_address(self.btc_currency)
            assert_equal(deposit_account, result_deposit_account, msg='Bittrex get_deposit_address')

    def test_get_all_deposit_addresses(self):
        # TODO: account not verified!!!
        result_deposit_addresses = dict()

        for dict_currency in dict(requests.get('https://bittrex.com/api/v1.1/public/getcurrencies').json())\
                .get('result'):
            currency = dict(dict_currency).get('Currency')
            response = dict(self.get_request('/v1.1/account/getdepositaddress', '&currency=' + currency +
                                             '&currencyname=' + currency))
            if response.get('result'):
                result_deposit_addresses[Currency(currency)] = dict(response.get('result')).get('Address')

        if not result_deposit_addresses:
            result_deposit_addresses = None

        if result_deposit_addresses is None:
            with assert_raises(ExchangeNotSupport):
                self.provider.get_all_deposit_addresses()
        else:
            deposit_addresses = self.provider.get_all_deposit_addresses()
            assert_equal(deposit_addresses, result_deposit_addresses, msg='Bittrex get_all_deposit_addresses')

    def test_get_indirect_price(self):
        price = self.provider.get_indirect_price(self.DASH, self.ETH, self.date_time)
        assert_equal(Decimal("0.91000000000000003108624468950438313186168670654296875"), price, msg="Test get_price with all valid parameters")

        price = self.provider.get_indirect_price(self.xmr_currency, self.DASH, self.date_time)
        assert_equal(Decimal("0.3149553186813186598153975216"), price, msg="Idex get_indirect_price")

    def _test_get_candles(self):
        dt = datetime(year=2018, month=5, day=1, hour=0, minute=0, second=0)

        for interval in self.provider.get_valid_intervals():
            candles = self.provider.get_candles(Currency.BTC, Currency.USD, term=dt, interval=interval)

            for candle in reversed(candles):
                print(dt - candle.date_time)

            expect = json.loads(read_text(res, res.candles_file.format(interval.total_seconds())))

            for index, expected_candle in enumerate(expect):
                candle = {name: str(value) for name, value in asdict(candles[index]).items()}
                assert_equal(expected_candle, candle)
