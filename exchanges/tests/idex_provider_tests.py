import unittest
from datetime import datetime
from decimal import Decimal
from functools import reduce
from pathlib import Path

import requests
from nose.tools import assert_equal, assert_raises, assert_in
from pytz import utc

from ..objects.currency import Currency
from ..objects.factories import OrderFactory, MarketFactory
from ..objects.market import Market
from ..providers.idex_provider import IdexProvider
from ..services.exception import ExchangeNotSupport, EmptyResult
from ..storage.file_storage import FileStorage


class IdexProviderTests(unittest.TestCase):

    def __init__(self, methodName='runTest'):

        self.api_key = '0x6d3d050d2b152494c2563565a2f9c58815569b09f0598917057e8d51306877ad'
        self.address = '0x4bf1057e83b7b328de8f02e8217aa5e36304ba51'
        fs = FileStorage(Path.cwd() / "data_source" / "cash_data" / "cash.json")
        self.idex_provider = IdexProvider(address=self.address, key=self.api_key, cash_interface=fs)
        # 2017-10-11 21:41:15
        self.date_time = datetime(2018, 6, 13, 15, 28, 29)
        self.date_time_now = datetime.utcnow().replace(tzinfo=utc)
        self.eth_currency = Currency.get("ETH")
        self.rem_currency = Currency.get("REM")
        self.exmr_currency = Currency.get("EXMR")
        self.dash_currency = Currency.get("DASH")
        self.btc_currency = Currency.get("BTC")
        self._markets = None

        super().__init__(methodName)

    def post_request(self, command, signed=False, data=None):
        session = requests.session()
        session.headers.update({'Accept': 'application/json',
                                'User-Agent': 'python-idex'})
        request_url = 'https://api.idex.market/' + command
        response_req = session.post(request_url, None, data)
        response = response_req.json()
        return response

    def test_get_price(self):
        # for using now time use utc timezone time like in poloniex
        price = self.idex_provider.get_price(self.rem_currency, self.eth_currency, self.date_time)
        assert_equal(price, Decimal("0.000031210000001719"), msg='Idex get_price')

        price = self.idex_provider.get_price(self.rem_currency, self.rem_currency, self.date_time)
        assert_equal(price, Decimal("1"), msg='Idex get_price')

        with assert_raises(ExchangeNotSupport):
            price = self.idex_provider.get_price(self.rem_currency, self.exmr_currency, self.date_time)

    def test_get_deposit_history(self):
        with assert_raises(ExchangeNotSupport):
            self.idex_provider.get_deposit_history(self.rem_currency,
                                                   self.date_time,
                                                   datetime.today())

    def test_get_withdraw_history(self):
        with assert_raises(ExchangeNotSupport):
            self.idex_provider.get_withdraw_history(self.rem_currency,
                                                    self.date_time,
                                                    datetime.today())

    def test_get_order_book(self):

        result_order_book = dict(self.post_request('returnOrderBook', False, {'market': 'ETH_REM'}))

        if result_order_book:
            market = Market(Currency.get('ETH'), Currency.get('REM'), '_')
            bids = map(lambda order: OrderFactory.from_idex(market, order, True), result_order_book.get('bids'))
            asks = map(lambda order: OrderFactory.from_idex(market, order, False), result_order_book.get('asks'))
            tuple_order_book = tuple(bids) + tuple(asks)
        else:
            tuple_order_book = None

        if tuple_order_book is None:
            with assert_raises(ExchangeNotSupport):
                self.idex_provider.get_order_book(self.eth_currency, self.rem_currency)
        else:
            order_book = self.idex_provider.get_order_book(self.eth_currency, self.rem_currency)

            for order in tuple_order_book:
                assert_in(order, order_book, msg='Idex get_order_book')

    def test_get_open_orders(self):
        open_orders = self.idex_provider.get_my_open_orders(self.rem_currency)
        assert_equal(open_orders, list(), msg='Idex get_open_orders')

    def test_get_trading_deals(self):
        with assert_raises(EmptyResult):
            self.idex_provider.get_trading_deals(self.eth_currency)

    def test_get_exchange_currencies(self):
        exchange_currencies = self.idex_provider.get_currencies()

        markets = tuple(MarketFactory.from_idex(ticker)
                        for ticker in dict(self.post_request('returnTicker')).keys())
        result_exchange_currencies = reduce(lambda res, market: res.union(market.currencies), markets, set())
        for exchange in exchange_currencies:
            assert_in(exchange, result_exchange_currencies, msg='Idex get_exchange_currencies')

    def test_get_balance(self):
        balance = self.idex_provider.get_balance(self.eth_currency)
        assert_equal(balance, Decimal("0"), msg='Idex get_balance')

        balance = self.idex_provider.get_balance(self.rem_currency)
        assert_equal(balance, Decimal("0"), msg='Idex get_balance')

    def test_get_deposit_address(self):
        with assert_raises(ExchangeNotSupport):
            self.idex_provider.get_deposit_address(self.rem_currency)

    def test_get_all_deposit_addresses(self):
        with assert_raises(ExchangeNotSupport):
            self.idex_provider.get_all_deposit_addresses()

    def test_get_indirect_price(self):
        price = self.idex_provider.get_indirect_price(self.rem_currency, self.eth_currency, self.date_time)
        assert_equal(price, Decimal("0.000031210000001719"), msg='Idex get_price')

        price = self.idex_provider.get_indirect_price(self.exmr_currency, self.rem_currency, self.date_time)
        assert_equal(Decimal("0.4466227819364481969089868347"), price, msg="Idex get_indirect_price")
