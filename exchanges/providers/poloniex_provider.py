import logging
from datetime import datetime, timedelta
from decimal import Decimal
from functools import reduce, lru_cache
from typing import Tuple, Iterable, Union

from pytz import utc

from ..api import PoloniexAPI
from ..api.cash.cash_poloniex import CashPoloniexAPI

from ..objects import Currency, Deal, DealFactory, Market, MarketFactory, Order, OrderFactory, CandleFactory
from ..providers import preprocess_open_order
from ..services.exception import ExchangeNotSupport, EmptyResult
from ..storage import CashStorageInterface

from .service import ExchangeBase, preprocess_get_price


class PoloniexProvider(ExchangeBase):
    __delimiter__ = "_"
    __is_swap__: bool = False

    def __init__(self, key, secret, cash_interface: CashStorageInterface=None):
        """

        :param key:
        :param secret:
        """
        self.api = PoloniexAPI(api_secret=secret, api_key=key, ) if cash_interface is None else CashPoloniexAPI(api_secret=secret, api_key=key, cash_interface=cash_interface)
        self._markets = None
        super().__init__(key, secret)

    @staticmethod
    def _to_order(market, order, is_buy) -> Order:
        return OrderFactory.from_poloniex(market, order, is_buy)

    @staticmethod
    def _to_market(market) -> Market:
        return MarketFactory.from_poloniex(market)

    @staticmethod
    def _to_deal(market, deal) -> Deal:
        return DealFactory.from_poloniex(market, deal)

    def get_current_price(self, sold: Currency, sale: Currency):
        market = self._get_valid_market(sold, sale)

        trades = list(self.api.publicReturnTradeHistory(currencyPair=str(market)))

        if not trades:
            raise ExchangeNotSupport("Exchange return empty result.")

        return Decimal(trades[0].get('rate'))

    def get_candles(self, sold: Currency, sale: Currency, term: Union[timedelta, Tuple[datetime, datetime]], interval: timedelta):
        if interval not in self.__valid_intervals__:
            raise ExchangeNotSupport(f"Can't get candles with intervals not from {self.__valid_intervals__}")

        market = self._get_valid_market(sold, sale)

        if isinstance(term, timedelta):
            term = datetime.now() - term, datetime.now()

        candles = self.api.returnChartData(str(market), term[0].timestamp(), term[1].timestamp(), interval.seconds)
        candles = map(lambda candle: CandleFactory.from_poloniex(candle, interval=interval), candles)

        return tuple(candles)

    @preprocess_get_price
    def get_price(self, sold: Currency, sale: Currency, date_time: datetime = None) -> Decimal:
        """

        :param sold:
        :param sale:
        :param date_time: Need to set UNIX timestamp in UTC
        :return: may be None
        """
        market = self._get_valid_market(sold, sale)

        if market not in self.get_markets():
            raise ExchangeNotSupport(f"Don't find this market {str(market)} on exchange.")

        if not date_time:
            return self.get_current_price(sold, sale)

        trades: list = self.api.publicReturnTradeHistory(currencyPair=str(market),
                                                         start=int((date_time - timedelta(days=1)).timestamp()),
                                                         end=int(date_time.timestamp()))

        if not trades:
            raise ExchangeNotSupport(f"Exchange return empty result")

        trade = min(trades, key=lambda tr: abs(
            date_time.timestamp() - datetime.strptime(tr.get("date"), "%Y-%m-%d %H:%M:%S").timestamp()))
        return Decimal(trade.get('rate'))

    def get_deposit_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        """

        :param end:
        :param start:
        :param currency:
        :return:
        """
        deposits_withdrawals = self.api.returnDepositsWithdrawals(start.timestamp(), end.timestamp())
        try:
            deposits = dict(deposits_withdrawals).get('deposits')
            filtered_deposits = tuple(map(lambda filtered_deposit: (filtered_deposit.get('address'),
                                                                    filtered_deposit.get('amount'),
                                                                    filtered_deposit.get('timestamp'),
                                                                    filtered_deposit.get('status'),
                                                                    filtered_deposit.get('txid')),
                                          filter(lambda deposit: deposit.get('currency') == currency.ticker, deposits)))

            if len(tuple(filtered_deposits)) > 0:
                return filtered_deposits

        except Exception:
            raise ExchangeNotSupport

        raise ExchangeNotSupport

    def get_withdraw_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        """

        :param end:
        :param start:
        :param currency:
        :return:
        """
        deposits_withdrawals = self.api.returnDepositsWithdrawals(start.timestamp(), end.timestamp())
        try:
            withdraws = dict(deposits_withdrawals).get('withdrawals')
            filtered_withdraws = tuple(map(lambda filtered_withdraw: (filtered_withdraw.get('address'),
                                                                      filtered_withdraw.get('amount'),
                                                                      filtered_withdraw.get('timestamp'),
                                                                      filtered_withdraw.get('status'),
                                                                      filtered_withdraw.get('txid')),
                                           filter(lambda withdraw: withdraw.get('currency') == currency.ticker,
                                                  withdraws)))

            if len(tuple(filtered_withdraws)) > 0:
                return filtered_withdraws

        except Exception:
            raise ExchangeNotSupport

        raise ExchangeNotSupport

    def get_order_book(self, sold: Currency, sale: Currency) -> tuple:
        """

        :param sold:
        :param sale:
        :return:
        """
        market = self._get_valid_market(sold, sale)

        if not self.is_market_present(market=market, is_reverse=False):
            raise ExchangeNotSupport

        order_book = dict(self.api.returnOrderBook(currencyPair=str(market)))

        if order_book:
            bids = map(lambda order: OrderFactory.from_poloniex(market, order, True), order_book.get('bids'))
            asks = map(lambda order: OrderFactory.from_poloniex(market, order, False), order_book.get('asks'))

            return tuple(bids) + tuple(asks)

        raise ExchangeNotSupport

    def get_my_open_orders(self, currency: Currency) -> tuple:
        """

        :param currency: may be None
        :return:
        """
        result_open_orders = []
        # Get possibles markets
        markets = self.get_markets()

        if currency is not None:
            markets = filter(lambda m: currency in m, markets)

        for market in markets:

            for order in tuple(dict(self.api.returnOpenOrders(currencyPair=str(market)))):
                order_list = (dict(order).get('rate'), dict(order).get('amount'))
                is_buy = not dict(order).get('type') == 'sell'

                result_open_orders.append(OrderFactory.from_poloniex(market, order_list, is_buy))

        return tuple(result_open_orders)

    def get_trading_deals(self, currency: Currency) -> tuple:
        """

        :param currency: may be None
        :return: may be None
        """
        date_time_now = datetime.utcnow().replace(tzinfo=utc)
        result_trade_deals = []
        markets = self.get_markets()

        if currency is not None:
            markets = filter(lambda m: currency in m, markets)

        for market in markets:
            # return trades for one day without start and/not and end. Msx return 10k results
            trades = self.api.tradeReturnTradeHistory(currencyPair=str(market),
                                                      start=int(date_time_now.timestamp() - 30 * 86400),
                                                      end=int(date_time_now.timestamp()),
                                                      limit=10000)

            if not trades:
                continue

            for trade in tuple(trades):
                result_trade_deals.append(self._to_deal(market, trade))

        return tuple(result_trade_deals)

    def get_currencies(self) -> tuple:
        """

        :return:
        """
        return tuple(reduce(lambda res, market: res.union(market.currencies), self.get_markets(), set()))

    def get_markets(self):
        if self._markets is None or len(self._markets) == 0:
            self._markets = tuple(MarketFactory.from_poloniex(ticker)
                                  for ticker in self.api.returnTicker())

        return self._markets

    def get_balance(self, currency: Currency) -> Decimal:
        """

        :param currency:
        :return: may be None
        """
        # This value may be None if not currency in available currencies
        balance = dict(self.api.returnBalances()).get(currency.ticker)
        if balance:
            return Decimal(balance)

        logging.info("Poloniex provider: no info for currency %s. Returning Decimal('0');" % currency.name)
        return Decimal('0')

    def get_balances(self):
        balances = self.api.returnBalances()
        return {Currency.get(currency): Decimal(count) for currency, count in balances.items() if float(count) != 0}

    def get_deposit_address(self, currency: Currency) -> str:
        """

        :param currency: required
        :return:
        """
        deposit_addresses = dict(self.api.returnDepositAddresses())
        if deposit_addresses and currency.ticker in deposit_addresses.keys():
            return deposit_addresses.get(currency.ticker, None)
        raise ExchangeNotSupport

    def get_all_deposit_addresses(self) -> dict:
        """

        :return: may be None
        """
        deposit_addresses = dict(self.api.returnDepositAddresses())

        if deposit_addresses:
            return {Currency.get(currency): address for currency, address in deposit_addresses.items()}

        raise EmptyResult()

    @preprocess_open_order
    def open_order(self, sold: Currency, sale: Currency, price: Decimal, quantity: Decimal, is_buy_type: bool):
        market = Market(sold, sale, '_')
        order = self.api.buy(market, price, quantity) if is_buy_type else self.api.sell(market, price, quantity)
        if order.get('orderNumber', None) is None:
            raise ExchangeNotSupport("Don't open order")

        return Order(market, price, quantity, is_buy_type)

    def cancel_order(self, uid):
        if not self.api.returnOrderTrades(uid):
            raise ExchangeNotSupport("Not support order number")

        if not self.api.cancelOrder(uid).get('success'):
            raise ExchangeNotSupport("Error in canceling order")

        logging.log("Success cancel order: {0}", uid)

    @lru_cache(maxsize=32)
    def get_middle_currencies(self, sold: Currency, sale: Currency) -> tuple:

        middle_currencies = reduce(lambda res, market: res.union(market.currencies),
                                   filter(lambda market: sale in market or sold in market,
                                          self.get_markets()),
                                   set())

        def filter_middle(currency):
            return currency not in (sold, sale) and \
                   self.is_currency_traded(sold, currency) and \
                   self.is_currency_traded(currency, sale)

        return tuple(filter(filter_middle, middle_currencies))

    def withdraw(self, currency: Currency, count: Decimal, address: str):
        result = self.api.withdraw(currency, count, address).get('response', None)
        if not result:
            raise ExchangeNotSupport("Error in withdrawing")

        logging.info(msg=result)

    def get_valid_intervals(self) -> Iterable[timedelta]:
        return self.__valid_intervals__

    __valid_intervals__ = timedelta(minutes=5), \
                          timedelta(minutes=10), \
                          timedelta(minutes=30), \
                          timedelta(hours=2), \
                          timedelta(hours=4), \
                          timedelta(days=1)
