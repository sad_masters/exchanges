from decimal import Decimal
from datetime import timedelta, datetime
from enum import Enum
from typing import Optional, Iterable, Union, Tuple, Dict, Sized, List, Generator

from exchanges.api.pseudo_api import PseusoApi, PseudoDeal
from exchanges import ExchangeInterface, Candle, Order, Market, ExchangeNotSupport, range_datetime


class Currency(Enum):
    PSD1 = "Pseudo1"
    PSD2 = "Pseudo2"


class PseudoProvider(ExchangeInterface):
    def __init__(self, start_price: Decimal, term: Union[timedelta, Tuple[datetime, datetime]],
                 balances: Dict[Currency, Decimal] = None):
        self._start_price: Decimal = start_price
        self._balances: dict = balances or {}

        self._api = PseusoApi.generate_deals(start_price, term)
        self._deals: List[PseudoDeal] = [next(self._api), ]

    def _init_new_deal(self) -> datetime:
        self._deals.append(next(self._api))
        return self._deals[-1].dt

    def _update_to_dt(self, end: datetime):

        while self._deals[-1].dt <= end:
            self._init_new_deal()

    def _get_deal(self, dt: datetime):
        last = self._deals[-1]
        for deal in reversed(self._deals):
            if deal.dt > dt:
                return last
            last = deal
        return last

    def get_price(self, sold: Currency, sale: Currency, date_time: datetime = None) -> Decimal:

        self._update_to_dt(date_time)
        price = self._get_deal(date_time).price

        if sold is Currency.PSD1 and sale is Currency.PSD2:
            return price
        elif sold is Currency.PSD2 and sale is Currency.PSD1:
            return Decimal('1') / price
        else:
            raise ValueError("ToDo")

    def get_candles(self,
                    sold: Currency,
                    sale: Currency,
                    term: Union[Tuple[datetime, datetime], timedelta],
                    interval: timedelta) -> Generator[Candle, None, None]:

        if isinstance(term, timedelta):
            term = datetime.now() - term, datetime.now()

        self._update_to_dt(term[1])
        for current in range_datetime(term[0], term[1], interval):
            _deals: PseudoDeal = tuple(filter(lambda d: current > d.dt > current + interval, self._deals))
            prices = tuple(map(lambda deal: deal.price, _deals))

            yield Candle(close=_deals[-1].price,
                         open=_deals[0].price,
                         volume=sum(map(lambda deal: deal.volume, _deals)),
                         high=max(prices),
                         low=min(prices),
                         date_time=current)

    def is_currency_traded(self, sold: str, sale: str):
        return sold in self.get_currencies() and sale in self.get_currencies()

    def get_currencies(self) -> Iterable[Currency]:
        return Currency.PSD1, Currency.PSD2

    def get_current_price(self, currency_sold: Currency, currency_sale: Currency) -> Decimal:
        return self.get_price(currency_sold, currency_sale, datetime.now())

    def get_balance(self, currency: Currency) -> Decimal:
        return self._balances.get(currency, Decimal('0'))

    def get_balances(self):
        return self._balances

    def get_markets(self) -> tuple:
        return Market(Currency.PSD1, Currency.PSD2, ','),

    def get_deposit_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        raise ExchangeNotSupport()

    def get_withdraw_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        raise ExchangeNotSupport()

    def get_trading_deals(self, currency: Currency) -> tuple:
        raise ExchangeNotSupport()

    def get_order_book(self, currency_first: Currency, currency_second: Currency) -> tuple:
        raise ExchangeNotSupport()

    def get_my_open_orders(self, currency: Currency) -> list:
        raise ExchangeNotSupport()

    def get_deposit_address(self, currency: Currency) -> str:
        raise ExchangeNotSupport()

    def get_all_deposit_addresses(self) -> dict:
        raise ExchangeNotSupport()

    def get_indirect_price(self, sold: Currency, sale: Currency, date_time: datetime = None):
        raise ExchangeNotSupport()

    def buy(self, sold: Currency, sale: Currency, quantity: Decimal, price: Optional[Decimal]) -> Order:
        raise ExchangeNotSupport()

    def sell(self, sold: Currency, sale: Currency, quantity: Decimal, price: Optional[Decimal]) -> Order:
        raise ExchangeNotSupport()

    def open_order(self, sold: Currency, sale: Currency, price: Decimal, quantity: Decimal, is_buy_type: bool) -> Order:
        raise ExchangeNotSupport()

    def cancel_order(self, uid):
        raise ExchangeNotSupport()

    def get_middle_currencies(self, currency_sold, currency_sale) -> tuple:
        raise ExchangeNotSupport()

    def withdraw(self, currency: Currency, count: Decimal, address: str) -> str:
        raise ExchangeNotSupport()

    def get_valid_intervals(self) -> Optional[Iterable[timedelta]]:
        raise ExchangeNotSupport()


if __name__ == '__main__':
    prv = PseudoProvider(Decimal('100'), timedelta(hours=100), {})
    price = prv.get_price(Currency.PSD1, Currency.PSD2, date_time=(datetime.now() - timedelta(minutes=10)))
    print(price)

    price = prv.get_price(Currency.PSD1, Currency.PSD2, date_time=(datetime.now() - timedelta(minutes=9)))
    print(price)
