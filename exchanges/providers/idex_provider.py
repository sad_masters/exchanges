import logging
from collections import namedtuple
from datetime import datetime, timedelta
from decimal import Decimal
from functools import reduce
from typing import Dict, Tuple, Iterable, Optional

from ..api.cash.cash_idex_api import CashIdexAPI
from ..api.idex_api import IdexAPI
from ..objects import Currency, Deal, DealFactory, CandleFactory, Market, MarketFactory, Order, OrderFactory
from ..providers import preprocess_open_order
from ..services.exception import ExchangeNotSupport, EmptyResult
from ..storage import CashStorageInterface
from .service import preprocess_get_price, ExchangeBase


class IdexProvider(ExchangeBase):
    __delimiter__: str = "_"
    __is_swap__: bool = True

    def __init__(self, key, secret, cash_interface: CashStorageInterface = None):
        """

        :param key: address from idex
        :param secret: secret key
        """
        self.api = IdexAPI(private_key=secret, address=key) if cash_interface is None \
            else CashIdexAPI(private_key=secret, address=key, cash_interface=cash_interface)

        self._markets = None
        super().__init__(key, secret)

    @staticmethod
    def _to_order(market, order, is_buy) -> Order:
        return OrderFactory.from_idex(market, order, is_buy)

    @staticmethod
    def _to_market(market) -> Market:
        return MarketFactory.from_idex(market)

    @staticmethod
    def _to_deal(market, deal) -> Deal:
        return DealFactory.from_idex(market, deal)

    __valid_intervals__ = True

    def get_trade_history(self, sold: Currency, sale: Currency, term: Tuple[datetime, datetime]):
        Trade = namedtuple('Trade', ('dt', 'price', 'amount', 'other'))

        def to_trade(raw: dict):
            return Trade(dt=datetime.fromtimestamp(raw.pop('timestamp')),
                         price=Decimal(raw.pop('price')),
                         amount=Decimal(raw.pop('amount')),
                         other=raw)

        raw = self.api.get_trade_history(market=str(self._get_valid_market(sold, sale)),
                                         start=int(term[0].timestamp()),
                                         end=int(term[1].timestamp()))

        return tuple(map(to_trade, raw))

    def get_candles(self,
                    sold: Currency,
                    sale: Currency,
                    term: Tuple[datetime, datetime] = None,
                    interval: timedelta = None):

        if interval > term[1] - term[0]:
            raise ValueError()

        trades = self.get_trade_history(sold, sale, term)
        candles = list()
        current = term[0]

        while current < term[1]:
            candle_trades = {t.dt: (t.price, t.amount) for t in trades if current < t.dt <= current + interval}

            if candle_trades:
                candle = CandleFactory.from_idex(candle_trades, interval=interval, start=current)
            else:
                price = candles[-1].close if candles else self.get_price(sold, sale, current)
                candle = CandleFactory.get_solo_candle(price=price,
                                                       date_time=current,
                                                       interval=interval)
            setattr(candle, '_trades', candle_trades)
            candles.append(candle)
            current += interval

        return tuple(candles)

    @preprocess_get_price
    def get_price(self, sold: Currency, sale: Currency, date_time: datetime = None) -> Decimal:
        """

        :param sold:
        :param sale:
        :param date_time:
        :return: may be None
        """
        if date_time is None:
            return self.get_current_price(sold, sale)

        market = self._get_valid_market(sold, sale)

        logging.debug("Idex start get_price with %s -> %s at %s" % (sold, sale, date_time))

        start = (date_time - timedelta(hours=24)).timestamp()
        end = (date_time + timedelta(hours=5)).timestamp()

        trade_history = self.api.get_trade_history(market=str(market),
                                                   address=None,
                                                   start=int(start),
                                                   end=int(end))
        if not trade_history:
            try:
                current_price = self.get_current_price(market.sold, market.sale)

            except ExchangeNotSupport:
                raise ExchangeNotSupport("Exchange return empty result.")

            return current_price

        trade = min(trade_history,
                    key=lambda tr: abs(date_time - datetime.strptime(tr.get("date"), "%Y-%m-%d %H:%M:%S")))

        return Decimal(trade.get('price'))

    def get_current_price(self, sold: Currency, sale: Currency):
        market = self._get_valid_market(sold, sale)

        logging.debug("Idex start get_price with %s -> %s" % (market.sold, market.sale))

        trade_history = self.api.get_trade_history(market=str(market),
                                                   address=None)

        if not trade_history:
            raise ExchangeNotSupport("Exchange return empty result")
        now = datetime.now()
        trade = min(trade_history,
                    key=lambda tr: abs(now - datetime.strptime(tr.get("date"), "%Y-%m-%d %H:%M:%S")))

        return Decimal(trade.get('price'))

    def get_deposit_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        """

        :param currency:
        :param start:
        :param end:
        :return:
        """
        address = self.api.get_wallet_address()
        deposits_withdrawals = self.api.get_transfers(address, int(start.timestamp()), int(end.timestamp()))
        try:
            deposits = deposits_withdrawals.get('deposits')
            filtered_deposits = map(lambda filtered_deposit: (address,
                                                              filtered_deposit.get('amount'),
                                                              filtered_deposit.get('timestamp'),
                                                              None,
                                                              filtered_deposit.get('transactionHash')),
                                    filter(lambda deposit: deposit.get('currency') == currency.ticker, deposits))

            if len(tuple(filtered_deposits)) > 0:
                return tuple(filtered_deposits)

        except Exception:
            raise ExchangeNotSupport

        raise ExchangeNotSupport

    def get_withdraw_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        """

        :param currency:
        :param start:
        :param end:
        :return:
        """
        address = self.api.get_wallet_address()
        deposits_withdrawals = self.api.get_transfers(address, int(start.timestamp()), int(end.timestamp()))
        try:
            withdraws = deposits_withdrawals.get('withdrawals')
            filtered_withdraws = map(lambda filtered_withdraw: (address,
                                                                filtered_withdraw.get('amount'),
                                                                filtered_withdraw.get('timestamp'),
                                                                None,
                                                                filtered_withdraw.get('transactionHash')),
                                     filter(lambda withdraw: withdraw.get('currency') == currency.ticker, withdraws))

            if len(tuple(filtered_withdraws)) > 0:
                return tuple(filtered_withdraws)

        except Exception:
            raise ExchangeNotSupport

        raise ExchangeNotSupport

    def get_order_book(self, sold: Currency, sale: Currency) -> tuple:
        """

        :param sold:
        :param sale:
        :return:
        """
        market = self._get_valid_market(sold, sale)

        order_book: dict = self.api.get_order_book(market=str(market))

        if order_book:
            bids = map(lambda order: self._to_order(market, order, True), order_book.get('bids'))
            asks = map(lambda order: self._to_order(market, order, False), order_book.get('asks'))

            return tuple(bids) + tuple(asks)

        raise ExchangeNotSupport

    def get_my_open_orders(self, currency: Currency) -> tuple:
        """

        :param currency: may be None
        :return:
        """

        result_open_orders = []
        # Get possibles markets
        markets = map(lambda m: self._to_market(m),
                      tuple(dict(self.api.get_tickers())))

        if currency is not None:
            markets = filter(lambda m: currency in m, markets)

        for market in markets:
            for order in tuple(dict(self.api.get_my_open_orders(market=str(market)))):
                is_buy = False
                if order['type'] == 'buy':
                    is_buy = True
                result_open_orders.append(self._to_order(market, order, is_buy))

        return tuple(result_open_orders)

    def get_trading_deals(self, currency: Currency) -> tuple:
        """

        :param currency:
        :return: may be None
        """
        result_trade_deals = []

        if currency is not None:
            if not self.is_currencies_present(currency):
                return ()

        trades = dict(self.api.get_my_trade_history())
        if not trades:
            logging.log("Idex provider: empty deals result;")
            raise EmptyResult

        for market, trades in ((self._to_market(market), trades) for market, trades in trades.items()):
            if currency is not None and currency not in market:
                continue

            for trade in trades:
                result_trade_deals.append(self._to_deal(market, trade))

        return tuple(result_trade_deals)

    def get_currencies(self) -> tuple:
        """

        :return:
        """
        return tuple(reduce(lambda res, market: res.union(market.currencies), self.get_markets(), set()))

    def get_markets(self):
        """
        Get all possible markets from Idex in value
        :return:
        """
        if self._markets is None or len(self._markets) == 0:
            self._markets = tuple(MarketFactory.from_idex(ticker)
                                  for ticker in dict(self.api.get_tickers()).keys())

        return self._markets

    def get_balance(self, currency: Currency) -> Decimal:
        """

        :param currency:
        :return: may be None
        """
        balances = dict(self.api.get_my_balances(complete=False))
        if balances != {}:
            return Decimal(balances.get(currency.ticker))

        logging.info("Idex provider: no info for currency %s. Returning Decimal('0');" % currency.name)
        return Decimal('0')

    def get_balances(self) -> Dict[Currency, Decimal]:
        balances = dict(self.api.get_my_balances(complete=False))
        return {Currency.get(balance[0]): Decimal(balance[1]) for balance in balances.items()}

    def get_deposit_address(self, currency: Currency) -> str:
        """

        :param currency:
        :return:
        """
        transfers = self.api.get_my_transfers()

        if not transfers:
            raise ExchangeNotSupport

        for deposit in list(dict(transfers).get('deposits')):
            if dict(deposit).get('currency') == currency.value:
                return dict(deposit).get('transactionHash')
        raise ExchangeNotSupport

    def get_all_deposit_addresses(self) -> dict:
        """

        :return:
        """
        result_deposit_addresses = dict()
        transfers = self.api.get_my_transfers()

        if not transfers:
            raise ExchangeNotSupport

        for deposit in list(dict(transfers).get('deposits')):
            currency = Currency.get(deposit['currency'])
            result_deposit_addresses[currency] = deposit['transactionHash']

        if result_deposit_addresses:
            return result_deposit_addresses
        else:
            raise ExchangeNotSupport

    @preprocess_open_order
    def open_order(self, sold: Currency, sale: Currency, price: Decimal, quantity: Decimal, is_buy_type: bool):
        order = self.api.create_order(str(sold), str(sale), price, quantity)
        if order.get('orderNumber', None) is None:
            raise ExchangeNotSupport

        return Order(Market(sold, sale, '_'), price, quantity, True)

    def cancel_order(self, uid):
        if not self.api.get_order_trades(uid):
            raise ExchangeNotSupport("Not support order number")

        if not self.api.cancel_order(uid).get('success'):
            raise ExchangeNotSupport("Error in canceling order")

        logging.log("Success cancel order: {0}", uid)

    def get_middle_currencies(self, sold, sale) -> tuple:
        """
        Get currencies that contact with currency sold and sale.
        :param sold:
        :param sale:
        :return:
        """

        return Currency.ETH,

    def withdraw(self, currency: Currency, count: Decimal, address=None):
        currency_address = self.api.get_currency(currency).get('address', None)
        if currency_address is None:
            raise ExchangeNotSupport("Error in getting address of currency {0}", currency)

        if self.api.withdraw(count, currency_address) is None:
            raise ExchangeNotSupport("Error in withdrawing")

        logging.log("Withdrawed {0} {1}", str(count), currency)

    def get_valid_intervals(self) -> Optional[Iterable[timedelta]]:
        return None
