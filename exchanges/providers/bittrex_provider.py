import logging
from datetime import datetime, timedelta
from decimal import Decimal
from functools import reduce, lru_cache
from typing import Tuple, Union, Iterable

from ..api import BittrexAPI
from ..api.bittrex_api import using_requests
from ..api.cash.cash_bittrex_api import CashBittrexAPI
from ..api.service.bittrex_service import BittrexConstants

from ..objects import \
    Currency, \
    Deal, DealFactory, \
    Market, MarketFactory, \
    Order, OrderFactory, \
    Candle, CandleFactory
from ..providers import preprocess_open_order

from ..services import ExchangeNotSupport
from ..storage import CashStorageInterface

from .service import ExchangeBase, preprocess_get_price


class BittrexProvider(ExchangeBase):
    __delimiter__ = '-'
    __is_swap__: bool = True

    def __init__(self, key, secret, cash_interface: CashStorageInterface = None):
        """

        :param key:
        :param secret:
        """
        if cash_interface is None:
            self.api, self.api_2 = BittrexAPI(api_key=key,
                                              api_secret=secret,
                                              calls_per_second=1,
                                              dispatch=using_requests,
                                              api_version=BittrexConstants.API_V1_1.value), \
                                   BittrexAPI(api_key=key,
                                              api_secret=secret,
                                              calls_per_second=1,
                                              dispatch=using_requests,
                                              api_version=BittrexConstants.API_V2_0.value)
        else:
            self.api, self.api_2 = CashBittrexAPI(api_key=key,
                                                  api_secret=secret,
                                                  calls_per_second=1,
                                                  dispatch=using_requests,
                                                  api_version=BittrexConstants.API_V1_1.value,
                                                  cash_interface=cash_interface), \
                                   CashBittrexAPI(api_key=key,
                                                  api_secret=secret,
                                                  calls_per_second=1,
                                                  dispatch=using_requests,
                                                  api_version=BittrexConstants.API_V2_0.value,
                                                  cash_interface=cash_interface)

        super().__init__(key, secret)

        self._markets = None

    @staticmethod
    def _to_order(market, order, is_buy) -> Order:
        return OrderFactory.from_bittrex(market, order, is_buy)

    @staticmethod
    def _to_market(market) -> Market:
        return MarketFactory.from_bittrex(market)

    @staticmethod
    def _to_deal(market, deal) -> Deal:
        return DealFactory.from_bittrex(market, deal)

    def get_candles(self,
                    sold: Currency,
                    sale: Currency,
                    term: Union[datetime, Tuple[datetime, datetime]],
                    interval: timedelta) -> Tuple[Candle]:

        market = self._get_valid_market(sold, sale)

        if interval not in self.__valid_intervals__:
            raise ValueError(f"Can't get candles for {interval}")

        end = None
        if isinstance(term, tuple):
            end = term[1]
            term = term[0]

        candles = self.api_2.get_candles(market=str(market),
                                         tick_interval=self.__valid_intervals__[interval],
                                         time_stump=term.timestamp())

        if not candles.get("success"):
            raise ExchangeNotSupport("For this datetime exchange return empty result.")

        else:
            candles = candles.get("result")

        candles = map(lambda raw: CandleFactory.from_bittrex(raw, interval=interval), reversed(candles))

        if end is not None:
            candles = filter(lambda candle: candle.date_time < end, candles)

        return tuple(candles)

    @preprocess_get_price
    def get_price(self, sold: Currency, sale: Currency, date_time: datetime = None) -> Decimal:
        """

        :param sold:
        :param sale:
        :param date_time:
        :return: may be None
        """
        self._get_valid_market(sold, sale)

        if not date_time:
            return self.get_current_price(sold, sale)

        if datetime.now() < date_time:
            raise ExchangeNotSupport("The exchange does not give access to data from the future")

        logging.debug(f"Direct price: {sold.name} -> {sale.name} at {date_time}")

        delta = datetime.now() - date_time

        if delta < timedelta(days=9):
            tick_interval = timedelta(minutes=1)

        elif delta < timedelta(days=19):
            tick_interval = timedelta(minutes=5)

        elif delta < timedelta(days=39):
            tick_interval = timedelta(minutes=30)

        elif delta < timedelta(days=59):
            tick_interval = timedelta(hours=1)

        elif delta < timedelta(days=962):
            tick_interval = timedelta(days=1)

        else:
            raise ExchangeNotSupport("Can't get for old date time.")

        candles = self.get_candles(sold, sale, interval=tick_interval, term=date_time)
        candles = tuple(filter(lambda candle: candle.date_time <= date_time, candles))

        if not candles:
            raise ExchangeNotSupport("Can't get historical data or not market at this dt")

        candle: Candle = min(candles, key=lambda trade: date_time - trade.date_time)

        if (candle.date_time - date_time) < ((candle.date_time + tick_interval) - date_time):
            return candle.open

        return candle.close

    def get_current_price(self, sold: Currency, sale: Currency) -> Decimal:
        market = self._get_valid_market(sold, sale)

        logging.debug(f"Direct price: {sold.name} -> {sale.name}")

        trades = self.api.get_market_history(market=str(market)).get('result')

        if not trades:
            raise ExchangeNotSupport("Exchange return empty result")

        price = Decimal(trades[0].get('Price'))

        logging.debug(f"Find direct price: {sold.name} -> {sale.name}: {price}")

        return price

    def get_deposit_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        """

        :param currency:
        :param start:
        :param end:
        :return:
        """
        deposits_withdrawals = self.api.get_deposit_history(currency.ticker)
        try:
            deposits = deposits_withdrawals.get('result')
            success = deposits_withdrawals.get('success')
            filtered_deposits = tuple(map(lambda filtered_deposit: (filtered_deposit.get('Address'),
                                                                    filtered_deposit.get('Amount'),
                                                                    datetime.strptime("%Y-%m-%dT%H:%M:%S.%f",
                                                                                      filtered_deposit.get('Opened')),
                                                                    filtered_deposit.get('Canceled'),
                                                                    filtered_deposit.get('TxId')), deposits))

            if success:
                return filtered_deposits

        except Exception:
            raise ExchangeNotSupport

        raise ExchangeNotSupport

    def get_withdraw_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        """

        :param currency:
        :param start:
        :param end:
        :return:
        """
        deposits_withdrawals = self.api.get_withdrawal_history(currency.ticker)
        try:
            withdraws = deposits_withdrawals.get('result')
            success = deposits_withdrawals.get('success')
            filtered_withdraws = tuple(map(lambda filtered_withdraw: (filtered_withdraw.get('Address'),
                                                                      filtered_withdraw.get('Amount'),
                                                                      datetime.strptime("%Y-%m-%dT%H:%M:%S.%f",
                                                                                        filtered_withdraw.get(
                                                                                            'Opened')),
                                                                      filtered_withdraw.get('Canceled'),
                                                                      filtered_withdraw.get('TxId')), withdraws))

            if success:
                return filtered_withdraws

        except Exception:
            raise ExchangeNotSupport

        raise ExchangeNotSupport

    def get_order_book(self, sold: Currency, sale: Currency) -> tuple:
        """

        :param sold:
        :param sale:
        :return:
        """
        market = self._get_valid_market(sold, sale)

        if not self.is_market_present(market=market, is_reverse=False):
            raise ExchangeNotSupport

        # Parameter both using for getting order book for buy and sell
        order_book_for_market = dict(dict(self.api.get_orderbook(market=str(market),
                                                                 depth_type='both')).get('result'))

        buy = map(lambda order: self._to_order(market, order, True), order_book_for_market.get('buy'))
        sell = map(lambda order: self._to_order(market, order, False), order_book_for_market.get('sell'))

        return tuple(buy) + tuple(sell)

    def get_my_open_orders(self, currency: Currency) -> tuple:
        """

        :param currency: may be None
        :return:
        """
        result_open_orders = []
        # Get possibles markets
        markets = map(lambda m: self._to_market(m), self.api.get_markets()['result'])

        if currency is not None:
            markets = filter(lambda m: currency in m, markets)

        for market in markets:
            open_orders = dict(self.api.get_open_orders(market=str(market))).get('result')
            if open_orders:
                for order in tuple(open_orders):
                    result_open_orders.append(self._to_order(market, order, None))

        return tuple(result_open_orders)

    def get_trading_deals(self, currency: Currency) -> tuple:
        """

        :param currency: may be None
        :return:
        """
        result_trades = []

        markets = self.get_markets()

        if currency is not None:
            markets = filter(lambda m: currency in m, markets)

        # Get all possible markets
        for market in markets:
            trades = dict(self.api.get_market_history(market=str(market))).get('result')
            if not trades:
                continue

            for trade in trades:
                result_trades.append(self._to_deal(market, trade))

        return tuple(result_trades)

    def get_markets(self):
        """
        Get all possible markets from Bittrex.
        :return:
        """
        if self._markets is None:
            self._markets = tuple(MarketFactory.from_bittrex(ticker)
                                  for ticker in list(dict(self.api.get_markets()).get('result')))

        return self._markets

    def get_balance(self, currency: Currency) -> Decimal:
        """

        :param currency:
        :return: may be None
        """
        balance = dict(self.api.get_balance(currency=currency.ticker))
        if balance.get('result') and dict(balance.get('result')).get('Balance'):
            return Decimal(dict(balance.get('result')).get('Balance'))

        return Decimal('0')

    def get_balances(self):
        balances = self.api.get_balances().get('result', None)
        return {Currency.get(balance['Currency']): Decimal(balances['Balance']) for balance in balances}

    def get_deposit_address(self, currency: Currency) -> str:
        """

        :param currency:
        :return:
        """
        result = self.api.get_deposit_address(currency=currency.ticker)
        deposit_address = dict(result).get('result')
        if deposit_address:
            return dict(deposit_address).get('Address')
        else:
            raise ExchangeNotSupport

    def get_all_deposit_addresses(self) -> dict:
        """

        :return:
        """
        result_deposit_addresses = dict()

        for dict_currency in dict(self.api.get_currencies()).get('result'):

            currency = dict(dict_currency).get('Currency')
            response = dict(self.api.get_deposit_address(currency))

            if response.get('result'):
                result_deposit_addresses[Currency.get(currency)] = dict(response.get('result')).get('Address')

        if result_deposit_addresses:
            return result_deposit_addresses
        else:
            raise ExchangeNotSupport

    @preprocess_open_order
    def open_order(self, sold: Currency, sale: Currency, price: Decimal, quantity: Decimal, is_buy_type: bool):
        market = Market(sold, sale, '-')
        order = self.api.buy_limit(str(market), float(quantity), float(price)) \
            if is_buy_type else self.api.sell_limit(str(market), float(quantity), float(price))
        is_opened_order = order.get('success', None)
        if is_opened_order is None or not is_opened_order:
            raise ExchangeNotSupport

        return Order(market, price, quantity, is_buy_type)

    def cancel_order(self, uid):
        if not self.api.get_order(uid).get('success'):
            raise ExchangeNotSupport("Not support order number")

        if not self.api.cancel(uid).get('success'):
            raise ExchangeNotSupport("Error in canceling order")

        logging.log("Success cancel order: {0}", uid)

    @lru_cache(maxsize=32)
    def get_middle_currencies(self, sold: Currency, sale: Currency) -> tuple:
        """
        Get currencies that contact with currency sold and sale.
        :param sold:
        :param sale:
        :return:
        """
        middle_currencies = reduce(lambda res, market: res.union(market.currencies),
                                   filter(lambda market: sale in market or sold in market,
                                          self.get_markets()),
                                   set())

        def filter_middle(currency):
            return currency not in (sold, sale) and \
                   self.is_currency_traded(sold, currency) and \
                   self.is_currency_traded(currency, sale)

        middle = tuple(filter(filter_middle, middle_currencies))
        logging.debug(f"Find middle currencies: {sold.name} -> {sale.name} : {middle}")

        return middle

    def withdraw(self, currency: Currency, count: Decimal, address: str):
        result = self.api.withdraw(currency, float(count), address)
        if result.get('success', None) is None:
            raise ExchangeNotSupport("Error in withdrawing")

        logging.log("Withdrawed {0} {1} with uid: {2}", str(count), currency, result.get('result'))

    def get_valid_intervals(self) -> Iterable[timedelta]:
        return self.__valid_intervals__.keys()

    __valid_intervals__ = {timedelta(minutes=1): "oneMin", timedelta(minutes=5): "fiveMin",
                           timedelta(minutes=30): "thirtyMin", timedelta(hours=1): "hour", timedelta(days=1): "day"}
