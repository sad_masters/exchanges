import logging
from _md5 import md5

from datetime import datetime, timedelta
from decimal import Decimal
from functools import reduce

from typing import Tuple, Dict, List, Set

from ..storage import CashStorageInterface
from ..storage.main_storage import MainStorage
from ..services import ExchangeNotSupportAtDt, ExchangeNotSupport, EmptyResult

from ..objects import updated_currency, Currency
from ..objects.platforms import Platforms

from .service import ExchangeInterface, ExchangeBase


class Manager(ExchangeBase):
    def __init__(self, platform_user: Dict[Platforms, str], cash_interface: CashStorageInterface=None):
        self.cash_interface = cash_interface
        self._all_providers: List[ExchangeInterface] = list()
        self.__detached_providers: Tuple[ExchangeInterface] = None
        tuple(self.append_provider(platform, user) for platform, user in platform_user.items())

        super().__init__("", "")

    def get_candles(self, sold: Currency, sale: Currency, term: Tuple[datetime, datetime], interval: timedelta):
        raise ExchangeNotSupport("Can't get candles from few providers. Choose platform.")

    def append_provider(self, platform: Platforms, user: str):
        api, secret = MainStorage().get_keys(platform=platform, user=user)
        provider = platform.provider(key=api, secret=secret, cash_interface=self.cash_interface)

        platforms = self.platforms
        self._all_providers.append(provider)

        if platform not in platforms:
            self.__detached_providers = None

    @property
    def platforms(self) -> Set[Platforms]:
        return {Platforms.soft_get(provider) for provider in self._all_providers}

    def update_currency(self, is_local: bool = False):
        storage = MainStorage.get_local() if is_local else MainStorage()

        tuple(storage.insert_currency(title=title, ticker=ticker) for title, ticker in
              {"Bitcoin": "BTC", "Etherium": "ETH", "BNB": "BNB", "Tether": "USDT"}.items()
              if not Currency.contains(title))

        for currencies in map(lambda provider: provider.get_currencies(), self._detached_providers):
            for currency in set(filter(lambda cur: isinstance(cur, str), currencies)):
                logging.info(f"Manager new currency {currency}")
                storage.insert_currency(currency, currency)

        return updated_currency()

    def get_currencies_map(self):
        return {Platforms.get(provider): provider.get_currencies() for provider in self._detached_providers}

    def get_balances(self) -> Dict[Currency, Decimal]:
        balances: Dict[Currency, Decimal] = dict()

        for provider in self._all_providers:
            for currency, count in provider.get_balances().items():
                balances[currency] = balances.get(currency, Decimal('0')) + count

        return balances

    def get_estimated_balances(self, est_currency: Currency, date_time: datetime = None):
        return self.get_estimated_price(self.get_balances(), est_currency, date_time)

    def get_estimated_price(self, balances: Dict[Currency, Decimal], est_currency, date_time: datetime = None):

        def get_price(currency_balance):
            currency, balance = currency_balance
            return self.get_indirect_price(currency, est_currency, date_time) * balance

        return sum(map(get_price, balances.items()))

    def get_estimated_price_with_wait(self, balances: Dict[Currency, Decimal], est_currency,
                                      date_time: datetime = None):

        def get_price(currency_balance):
            currency, balance = currency_balance
            return self.get_indirect_price_with_wait(currency, est_currency, date_time) * balance

        return sum(map(get_price, balances.items()))

    def get_indirect_price_with_wait(self,
                                     sold: Currency,
                                     sale: Currency,
                                     date_time: datetime = None,
                                     max_delta: timedelta = None):
        if max_delta is None:
            max_delta = timedelta(days=365)

        ptr = date_time
        while date_time - ptr < max_delta:
            try:
                return self.get_indirect_price(sold, sale, ptr)

            except ExchangeNotSupport:
                ptr -= timedelta(days=1)
                continue

        raise ExchangeNotSupport(f"Can't find {sold} -> {sale} in term [{date_time}, {date_time - max_delta}]")

    def get_price(self, sold: Currency, sale: Currency, date_time: datetime = None, platform: Platforms = None):
        logging.info(f"Manager get_price with {sold} -> {sale} in {date_time} at {platform}")

        if sold is sale:
            return Decimal('1')

        prices = self.get_prices_map(sold, sale, date_time, platform)
        if not prices:
            raise ExchangeNotSupport(f"Can't find {sold} -> {sale} at {self.platforms}")

        return min(prices.values())

    def get_prices_map(self, sold: Currency, sale: Currency, date_time: datetime = None,
                       platforms: Tuple[Platforms] = None):
        logging.info(f"Manager tart get_price_map with {sold} -> {sale} in {date_time} at {date_time}")

        providers = self._detached_providers if platforms is None else self._get_providers(platforms)

        price_map = dict()
        for provider in filter(lambda p: p.is_currencies_present(sold, sale), providers):
            try:
                price_map[Platforms.soft_get(provider)] = provider.get_indirect_price(sold, sale, date_time)

            except ExchangeNotSupportAtDt:
                continue

        return price_map

    def get_middle_currencies(self, sold, sale) -> tuple:
        middle_currencies = reduce(lambda currencies, market: currencies.union(market.currencies),
                                   filter(lambda market: sale in market or sold in market,
                                          self.get_markets()),
                                   set())

        def filter_middle(currency):
            return currency not in (sold, sale) and \
                   self.is_currency_traded(sold, currency) and \
                   self.is_currency_traded(currency, sale)

        middle_currencies = tuple(filter(filter_middle, middle_currencies))
        logging.debug(f"Manager middle currencies {sold} -> {sale}: {middle_currencies}")
        return middle_currencies

    def get_markets(self, is_normal: bool=False) -> tuple:
        markets = reduce(lambda res, provider: res.union(provider.get_markets()), self._detached_providers, set())

        if is_normal:
            markets = map(lambda market: market.get_normal(), markets)

        return tuple(markets)

    def get_balance(self, currency: Currency) -> Decimal:
        return reduce(lambda res, provider: res + provider.get_balance(currency), self._all_providers, Decimal('0'))

    def get_indirect_price(self,
                           sold: Currency,
                           sale: Currency,
                           date_time: datetime = None) -> Decimal:
        try:
            return self.get_price(sold, sale, date_time)

        except ExchangeNotSupport:
            logging.debug("Manager can't find direct price for %s -> %s at %s. Start get_indirect_price" %
                          (sold, sale, date_time))

        prices = tuple(
            self.get_indirect_price(sold=sold, sale=currency, date_time=date_time) *
            self.get_indirect_price(sold=currency, sale=sale, date_time=date_time)

            for currency in self.get_middle_currencies(sold, sale))

        if not prices:
            raise ExchangeNotSupport(f"{sold.ticker} -> {sale.ticker} at {date_time}")

        return min(prices)

    def get_deposit_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        return tuple(
            reduce(lambda res, pr: res + pr.get_deposit_history(currency, start, end), self._all_providers, tuple()))

    def get_withdraw_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        return tuple(
            reduce(lambda res, pr: res + pr.get_withdraw_history(currency, start, end), self._all_providers, tuple()))

    def get_trading_deals(self, currency: Currency) -> tuple:
        result_tuple = tuple()
        for provider in self._all_providers:
            try:
                result_tuple += tuple(provider.get_trading_deals(currency))
            except EmptyResult:
                continue
        return result_tuple

    def get_trading_deals_map(self, currency: Currency, platforms: Tuple[Platforms]) -> dict:
        return {platform: self._get_provider(platform).get_trading_deals(currency) for platform in platforms}

    def is_currency_traded(self, sold, sale):
        for pr in self._detached_providers:
            if pr.is_currency_traded(sold, sale):
                return True

        return False

    def get_order_book(self, currency_first: Currency, currency_second: Currency) -> tuple:
        return tuple(reduce(lambda res, provider: res + provider.get_order_book(currency_first, currency_second),
                            self._detached_providers,
                            tuple()))

    def get_my_open_orders(self, currency: Currency) -> tuple:
        return tuple(reduce(lambda res, provider: res + provider.get_my_open_orders(currency),
                            self._all_providers,
                            tuple()))

    def get_deposit_address(self, currency: Currency) -> dict:
        platforms_deposit_addresses = dict()
        for provider in self._all_providers:
            try:
                deposit_address = provider.get_deposit_address(currency)
                platforms_deposit_addresses[Platforms.get(provider)] = deposit_address
            except ExchangeNotSupport:
                continue
        return platforms_deposit_addresses

    def get_all_deposit_addresses(self) -> dict:
        return {Platforms.get(provider): provider.get_all_deposit_addresses() for provider in self._all_providers}

    @property
    def _detached_providers(self) -> Tuple[ExchangeInterface]:

        if self.__detached_providers is None:
            self.__detached_providers: Tuple[ExchangeInterface] = \
                tuple({Platforms.get(provider): provider for provider in self._all_providers}.values())

        return self.__detached_providers

    def _get_provider(self, platform: Platforms, user: str = None) -> ExchangeInterface:
        h = None if user is None else md5("".join(MainStorage().get_keys(platform, user)))

        for provider in self._all_providers:
            if provider.__class__ == platform.provider and (h is None or (provider.hash == h)):
                return provider

        raise ValueError("Can't find provider for %s in this manager" % platform.name)

    def _get_providers(self, platforms: Tuple[Platforms], user: str = None):
        if isinstance(platforms, Platforms):
            platforms = (platforms,)
        return tuple(self._get_provider(platform, user) for platform in platforms)

    def get_current_price(self, currency_sold: Currency, currency_sale: Currency) -> Decimal:
        return self.get_price(currency_sold, currency_sale, datetime.now())
