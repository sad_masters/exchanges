import logging
from copy import copy
from datetime import datetime, timedelta
from decimal import Decimal
from functools import reduce, lru_cache
from typing import Tuple, Iterable, Union

from exchanges.exchanges.objects import Market, Order, OrderFactory, MarketFactory, Deal, DealFactory, Currency, CandleFactory


from ..api import BinanceAPI
from ..api.cash.cash_binance_api import CashBinanceAPI
from ..api.binance_api import BinanceConstants

from ..storage.interfaces.cash_storage_interface import CashStorageInterface
from ..providers import preprocess_open_order
from ..services.exception import ExchangeNotSupport, ExchangeNotSupportAtDt
from .service import preprocess_get_price, ExchangeBase


class BinanceProvider(ExchangeBase):
    __delimiter__ = ''
    __is_swap__: bool = False

    def __init__(self, key, secret, cash_interface: CashStorageInterface=None):
        """

        :param key:
        :param secret:
        """
        self.api = BinanceAPI(api_key=key,
                              api_secret=secret,
                              requests_params=None) if cash_interface is None \
            else CashBinanceAPI(key, secret, cash_interface)

        self._markets = None
        super().__init__(key, secret)

    @staticmethod
    def _to_order(market, order, is_buy) -> Order:
        return OrderFactory.from_binance(market, order, is_buy)

    @staticmethod
    def _to_market(market) -> Market:
        return MarketFactory.from_binance(market)

    def get_candles(self, sold: Currency, sale: Currency, term: Union[Tuple[datetime, datetime], None, timedelta],
                    interval: timedelta):
        market = self._get_valid_market(sold, sale)

        if interval not in self._valid_intervals_:
            raise ExchangeNotSupport("ToDo")  # ToDo

        if isinstance(term, timedelta):
            now = datetime.now()
            term = now - term, now

        if term is None:
            candles = self.api.get_klines(symbol=str(market), interval=self._valid_intervals_[interval], limit=1000)

        else:
            current = term[1]
            candles = []

            while current > term[0]:
                start, end = int((current - timedelta(hours=1)).timestamp()) * 1000, int(current.timestamp()) * 1000

                candles += self.api.get_klines(symbol=str(market), interval=self._valid_intervals_[interval],
                                               limit=1000, startTime=start, endTime=end)

                current -= timedelta(hours=1)

        candles = map(lambda candle: CandleFactory.from_binance(candle, interval=interval), candles)
        candles = filter(lambda candle: term[0] < candle.date_time < term[1], candles) if term is not None else candles

        return tuple(candles)

    @staticmethod
    def _to_deal(market, deal) -> Deal:
        return DealFactory.from_binance(market, deal)

    def _get_prices(self) -> dict:
        return {obj["symbol"]: obj["price"] for obj in self.api.get_all_tickers()}

    def get_current_price(self, sold: Currency, sale: Currency) -> Decimal:

        market = self._get_valid_market(sold, sale)

        logging.debug("start get_price with %s -> %s" % (market.sold, market.sale))

        prices = self._get_prices()

        if str(market) not in prices.keys():
            raise ExchangeNotSupport(f"Can'gt get price for {market.sold} -> {market.sale}")

        return Decimal(prices.get(str(market)))

    @preprocess_get_price
    def get_price(self, sold: Currency, sale: Currency, date_time: datetime = None) -> Decimal:
        """

        :param sold:
        :param sale:
        :param date_time:
        :return: may be None
        """

        if date_time is None:
            return self.get_current_price(sold, sale)

        market = self._get_valid_market(sold, sale)

        dt_ptr = copy(date_time)
        while date_time - dt_ptr < timedelta(hours=4):
            try:
                agg_trade = self._get_hour_trades(market, dt_ptr)
                trade = min(agg_trade, key=lambda t: abs(date_time.timestamp() - t["T"]))
                return Decimal(trade.get('p'))

            except ExchangeNotSupportAtDt:
                dt_ptr -= timedelta(hours=1)

        raise ExchangeNotSupportAtDt("Can't find trade for 24 hours at datetime %s" % date_time)

    def _get_hour_trades(self, market: Market, date_time: datetime):
        start = int((date_time - timedelta(minutes=59)).timestamp())
        end = int((date_time + timedelta(minutes=1)).timestamp())

        agg_trade: list = self.api.get_aggregate_trades(symbol=str(market),
                                                        startTime=start * 1000,
                                                        endTime=end * 1000)

        # At this datetime may not be trade with this currencies

        if len(agg_trade) == 0:
            raise ExchangeNotSupportAtDt("Exchange don't support at this datetime.")

        return agg_trade

    def get_deposit_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        """

        :param currency:
        :param start:
        :param end:
        :return:
        """
        deposits_withdrawals = self.api.get_deposit_history(asset=currency.ticker,
                                                            startTime=int(start.timestamp()) * 1000,
                                                            endTime=int(end.timestamp()) * 1000)
        try:
            deposits = deposits_withdrawals.get('depositList')
            success = deposits_withdrawals.get('success')
            filtered_deposits = tuple(map(lambda filtered_deposit: (None,
                                                                    filtered_deposit.get('amount'),
                                                                    filtered_deposit.get('insertTime'),
                                                                    filtered_deposit.get('status'),
                                                                    None), deposits))
            if success:
                return filtered_deposits

        except Exception:
            raise ExchangeNotSupport

        raise ExchangeNotSupport

    def get_withdraw_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        """

        :param currency:
        :param start:
        :param end:
        :return:
        """
        deposits_withdrawals = self.api.get_withdraw_history(asset=currency.ticker,
                                                             startTime=int(start.timestamp()) * 1000,
                                                             endTime=int(end.timestamp()) * 1000)
        try:
            withdraws = deposits_withdrawals.get('withdrawList')
            success = deposits_withdrawals.get('success')
            filtered_withdraws = tuple(map(lambda filtered_withdraw: (filtered_withdraw.get('address'),
                                                                      filtered_withdraw.get('amount'),
                                                                      filtered_withdraw.get('applyTime'),
                                                                      filtered_withdraw.get('status'),
                                                                      filtered_withdraw.get('txId') if 'txId' in list(
                                                                          filtered_withdraw.keys()) else None),
                                           withdraws))

            if success:
                return filtered_withdraws

        except Exception:
            raise ExchangeNotSupport

        raise ExchangeNotSupport

    def get_order_book(self, sold: Currency, sale: Currency) -> tuple:
        """

        :param sold:
        :param sale:
        :return:
        """
        market = self._get_valid_market(sold, sale)

        if not self.is_market_present(market=market, is_reverse=False):
            raise ExchangeNotSupport

        # Get order book: bids and asks
        order_book = dict(self.api.get_order_book(symbol=str(market), limit=1000))
        order_book.pop('lastUpdateId')

        # ToDo Vartan ToReview Не очень понял, почему все ордера на покупку

        bids = map(lambda order: self._to_order(market, order, is_buy=True), order_book.get('bids'))
        asks = map(lambda order: self._to_order(market, order, is_buy=False), order_book.get('asks'))

        return tuple(bids) + tuple(asks)

    def get_my_open_orders(self, currency: Currency) -> tuple:
        """

        :param currency: may be None
        :return:
        """

        # Get possibles markets
        markets = map(lambda mrk: self._to_market(mrk),
                      list(self.api.get_orderbook_tickers()))

        if currency is not None:
            markets = filter(lambda mrk: currency in mrk, markets)

        open_orders = []
        for market in markets:
            open_orders += \
                map(lambda order: self._to_order(market, order, None),
                    self.api.get_open_orders(symbol=str(market), recvWindow='300000'))

        return tuple(open_orders)

    def get_trading_deals(self, currency: Currency) -> tuple:
        """

        :param currency: may be None
        :return: may be None
        """
        markets = self.get_markets()

        if currency is not None:
            markets = filter(lambda mrk: currency in mrk, markets)

        trades = list()

        for market in markets:
            trades += map(lambda trade: self._to_deal(market, trade), self.api.get_my_trades(symbol=str(market)))

        return tuple(trades)

    @lru_cache(maxsize=1)
    def get_currencies(self) -> tuple:
        """

        :return:
        """
        return tuple(reduce(lambda res, market: res.union(market.currencies), self.get_markets(), set()))

    def get_markets(self):
        """
        Get all possible markets from Binance.
        :return:
        """
        if self._markets is None:
            self._markets = tuple(MarketFactory.from_binance(ticker)
                                  for ticker in list(self.api.get_all_tickers()))

        return self._markets

    def get_balance(self, currency: Currency) -> Decimal:
        """

        :param currency:
        :return: may be None
        """
        asset_balance = self.api.get_asset_balance(asset=currency.ticker)

        if asset_balance is None:
            return Decimal("0")

        return Decimal(dict(self.api.get_asset_balance(currency.ticker)).get('free'))

    def get_balances(self):
        balances = filter(lambda balance: float(balance['free']) + float(balance['locked']) != 0,
                          self.api.get_account().get('balances', ()))
        return {Currency.get(balance['asset']): Decimal(balance['free']) + Decimal(balance['locked'])
                for balance in balances}

    def get_deposit_address(self, currency: Currency) -> str:
        """

        :param currency:
        :return:
        """
        deposit_address = dict(self.api.get_deposit_address(asset=currency.ticker))
        address = deposit_address.get('address', None)
        if not address:
            raise ExchangeNotSupport
        return address

    @lru_cache(maxsize=1)
    def get_all_deposit_addresses(self) -> dict:
        """

        :return:
        """
        result_deposit_addresses = dict()

        for balance in self.api.get_account().get('balances', ()):

            asset = balance['asset']

            deposit_address = dict(self.api.get_deposit_address(asset=asset, recvWindow='60000'))

            if deposit_address:
                try:
                    currency = Currency.get(asset)
                    result_deposit_addresses[currency] = deposit_address.get('address')

                except ValueError:
                    logging.error(msg="Can't find currency with ticker by %s." % asset)
                    continue

        if not result_deposit_addresses:
            raise ExchangeNotSupport

        return result_deposit_addresses

    @preprocess_open_order
    def open_order(self, sold: Currency, sale: Currency, price: Decimal, quantity: Decimal, is_buy_type: bool):
        market = Market(sold, sale, '')
        order = self.api.order_limit_buy(symbol=str(market), quantity=quantity, price=str(price)) \
            if is_buy_type else self.api.order_limit_sell(symbol=str(market), quantity=quantity, price=str(price))
        order_status = order.get('status', None)
        if order_status is None or order_status in [BinanceConstants.ORDER_STATUS_CANCELED,
                                                    BinanceConstants.ORDER_STATUS_REJECTED]:
            raise ExchangeNotSupport("Don't open order")

        return Order(market, price, quantity, is_buy_type)

    def cancel_order(self, uid):
        order_status = self.api.get_order(orderId=uid).get('isWorking', None)
        if order_status is None or not order_status:
            raise ExchangeNotSupport("Not support order number")

        if self.api.cancel_order(orderId=uid).get('clientOrderId', None) is None:
            raise ExchangeNotSupport("Error in canceling order")

        logging.log("Success cancel order: {0}", uid)

    @lru_cache(maxsize=32)
    def get_middle_currencies(self, sold, sale) -> tuple:
        """
        Get currencies that contact with currency sold and sale.
        :param sold:
        :param sale:
        :return:
        """
        middle_currencies = reduce(lambda res, market: res.union(market.currencies),
                                   filter(lambda market: sale in market or sold in market,
                                          self.get_markets()),
                                   set())

        def filter_middle(currency):
            return currency not in (sold, sale) and \
                   self.is_currency_traded(sold, currency) and \
                   self.is_currency_traded(currency, sale)

        return tuple(filter(filter_middle, middle_currencies))

    def withdraw(self, currency: Currency, count: Decimal, address: str):
        if not self.api.withdraw(asset=currency, address=address, amount=count).get('success'):
            raise ExchangeNotSupport("Error in withdrawing")

        logging.log("Withdrawed {0} {1} to address {2}", count, currency, address)

    def get_valid_intervals(self) -> Iterable[timedelta]:
        return tuple(self._valid_intervals_.keys())

    _valid_intervals_ = {timedelta(minutes=1): '1m',
                         timedelta(minutes=3): '3m',
                         timedelta(minutes=5): '5m',
                         timedelta(minutes=15): '15m',
                         timedelta(minutes=30): '30m',
                         timedelta(hours=1): '1h',
                         timedelta(hours=2): '2h',
                         timedelta(hours=4): '4h',
                         timedelta(hours=6): '6h',
                         timedelta(hours=8): '8h',
                         timedelta(hours=12): '12h',
                         timedelta(days=1): '1d',
                         timedelta(days=3): '3d',
                         timedelta(days=7): '1w',
                         timedelta(days=30): '1M'}
