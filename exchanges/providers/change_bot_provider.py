from decimal import Decimal
from datetime import datetime
from enum import Enum
from ftplib import FTP

from ..services import ExchangeNotSupport
from ..objects import Currency
from .service import ExchangeInterface


class FtpConstants(Enum):
    BTC_BANKER_DIR = "path/to/dir/with/user/balance/info"


class ChangeBotProvider(ExchangeInterface):
    ftp_ip = ""
    username = ""
    password = ""

    def __init__(self, key: str, secret: tuple):
        # ToDo vg Need hash (see exchange_base) to work with provider as key
        self.ftp_ip = key

        if len(secret) != 2:
            raise Exception("Incorrect secret")

        self.username, self.password = secret

    def _get_balance_from_data(self, file_content_arr: list, currency: Currency) -> Decimal:
        return Decimal()

    def get_balance(self, currency: Currency) -> Decimal:
        with FTP(host=self.ftp_ip, user=self.username, passwd=self.password) as ftp_connection:
            ftp_connection.login()
            ftp_connection.cwd(FtpConstants.BTC_BANKER_DIR)
            data = ftp_connection.nlst()

            file_content_arr = []

            def callback_handle_csv_file(more_data):
                file_content_arr.append(more_data)

            ftp_connection.retrlines('RETR ' + data, callback=callback_handle_csv_file)

            balance = self._get_balance_from_data(file_content_arr, currency)
        return balance

    def get_balances(self):
        raise NotImplemented()

    def get_markets(self) -> tuple:
        raise NotImplemented()

    def get_deposit_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        raise NotImplemented()

    def get_withdraw_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        raise NotImplemented()

    def get_trading_deals(self, currency: Currency) -> tuple:
        raise NotImplemented()

    def get_deposit_address(self, currency: Currency) -> str:
        raise NotImplemented()

    def get_all_deposit_addresses(self) -> dict:
        raise NotImplemented()

    def get_order_book(self, currency_first: Currency, currency_second: Currency) -> tuple:
        raise ExchangeNotSupport()

    def get_price(self, currency_sold: Currency, currency_sale: Currency, date_time: datetime = None) -> Decimal:
        raise ExchangeNotSupport()

    def get_current_price(self, currency_sold: Currency, currency_sale: Currency) -> Decimal:
        raise ExchangeNotSupport()

    def get_my_open_orders(self, currency: Currency) -> list:
        raise ExchangeNotSupport()

    def get_indirect_price(self, sold: Currency, sale: Currency, date_time: datetime = None):
        raise ExchangeNotSupport()

    def open_order(self, sold_currency: Currency, sold_bought: Decimal, price: Decimal):
        raise ExchangeNotSupport()

    def cancel_order(self, uid):
        raise ExchangeNotSupport()

    def get_middle_currencies(self, currency_sold, currency_sale) -> tuple:
        raise ExchangeNotSupport()

    def withdraw(self, currency: Currency, count: Decimal, address: str) -> str:
        raise ExchangeNotSupport()
