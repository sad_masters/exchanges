import logging
from abc import abstractmethod
from datetime import datetime
from decimal import Decimal
from functools import reduce
from typing import Optional

from ...objects import Currency, Market
from ...services import ExchangeNotSupport

from .decorators import preprocess_indirect_get_price
from .exchange_interface import ExchangeInterface


class ExchangeBase(ExchangeInterface):
    __delimiter__ = "|"
    __is_swap__ = None

    def _get_valid_market(self, sold: Currency, sale: Currency, is_check: bool=True):
        market = Market(sold, sale, self.__delimiter__)
        if self.__is_swap__:
            market = market.get_swap()

        if is_check and not self.is_market_present(market, False):
            raise ExchangeNotSupport(f"Can't get price for {market.sold} -> {market.sale}")

        return market

    @abstractmethod
    def __init__(self, key: str, secret: str):
        super().__init__(key=key, secret=secret)

        from _md5 import md5
        self._hash = md5(key.encode('utf-8') + secret.encode('utf-8'))

    @property
    def hash(self):
        return self._hash

    @abstractmethod
    def get_price(self, currency_sold: Currency, currency_sale: Currency, date_time: datetime = None):
        pass

    def get_currencies(self) -> tuple:
        return tuple(reduce(lambda res, market: res.union(market.currencies), self.get_markets(), set()))

    @abstractmethod
    def get_balance(self, currency: Currency) -> Decimal:
        pass

    @abstractmethod
    def get_balances(self):
        pass

    @abstractmethod
    def get_markets(self) -> tuple:
        pass

    @abstractmethod
    def get_deposit_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        pass

    @abstractmethod
    def get_withdraw_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        pass

    @abstractmethod
    def get_trading_deals(self, currency: Currency) -> tuple:
        pass

    @abstractmethod
    def get_order_book(self, currency_first: Currency, currency_second: Currency) -> tuple:
        pass

    @abstractmethod
    def get_my_open_orders(self, currency: Currency) -> list:
        pass

    @abstractmethod
    def get_deposit_address(self, currency: Currency) -> str:
        pass

    @abstractmethod
    def get_all_deposit_addresses(self) -> dict:
        pass

    @preprocess_indirect_get_price
    def get_indirect_price(self,
                           sold: Currency,
                           sale: Currency,
                           date_time: datetime = None):
        """

        :param sold:
        :param sale:
        :param date_time:
        :return:
        """
        market = self._get_valid_market(sold, sale, is_check=False)

        if self.is_market_present(market):
            return self.get_price(sold, sale, date_time)

        logging.debug(f"Indirect price: {sold.name} -> {sale.name} at {date_time}")
        prices = tuple(
            self.get_indirect_price(sold=sold, sale=currency, date_time=date_time) *
            self.get_indirect_price(sold=currency, sale=sale, date_time=date_time)

            for currency in self.get_middle_currencies(market.sold, market.sale))

        if not prices:
            raise ExchangeNotSupport()

        logging.debug(f"Find indirect prices: {sold.name} -> {sale.name} at {date_time}: {prices}")

        return min(prices)

    def buy(self, sold: Currency, sale: Currency, quantity: Decimal, price: Optional[Decimal]):
        if price:
            return self.open_order(sold, sale, price, quantity, True)
        # TODO: PriceValidator
        raise NotImplemented

    def sell(self, sold: Currency, sale: Currency, quantity: Decimal, price: Optional[Decimal]):
        if price:
            return self.open_order(sold, sale, price, quantity, False)
        # TODO: PriceValidator
        raise NotImplemented

    def is_currencies_present(self, *currencies: tuple) -> bool:

        for currency in currencies:
            if currency not in self.get_currencies():
                return False

        return True

    def is_market_present(self, market: Market, is_reverse=True) -> bool:
        """
        Verify that market present in Exchange.
        :param is_reverse:
        :param market:
        :return:
        """
        direct = market in self.get_markets()
        reverse = is_reverse and market.get_swap() in self.get_markets()
        return direct or reverse or market.is_pseudo

    def is_currency_traded(self, sold, sale):
        return self.is_market_present(Market(sold, sale, self.__delimiter__))

    def name(self):
        from exchanges.objects.platforms import Platforms
        return Platforms.get_name(self)
