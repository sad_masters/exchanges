from decimal import Decimal
from abc import abstractmethod
from datetime import datetime, timedelta
from typing import Tuple, Iterable, Union, Optional

from exchanges.exchanges.objects import Market, Order
from exchanges.exchanges.objects.currency import Currency
from exchanges.exchanges.objects.candle import Candle


class ExchangeInterface:
    @abstractmethod
    def __init__(self, key: str, secret: str):
        pass

    @abstractmethod
    def is_currency_traded(self, sold, sale):
        pass

    @abstractmethod
    def get_price(self, currency_sold: Currency, currency_sale: Currency, date_time: datetime = None) -> Decimal:
        pass

    @abstractmethod
    def get_currencies(self) -> Tuple[Currency]:
        pass

    @abstractmethod
    def get_current_price(self, currency_sold: Currency, currency_sale: Currency) -> Decimal:
        pass

    @abstractmethod
    def get_balance(self, currency: Currency) -> Decimal:
        pass

    @abstractmethod
    def get_balances(self):
        pass

    @abstractmethod
    def get_markets(self) -> tuple:
        pass

    @abstractmethod
    def get_deposit_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        pass

    @abstractmethod
    def get_withdraw_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        pass

    @abstractmethod
    def get_trading_deals(self, currency: Currency) -> tuple:
        pass

    @abstractmethod
    def get_order_book(self, currency_first: Currency, currency_second: Currency) -> tuple:
        pass

    @abstractmethod
    def get_my_open_orders(self, currency: Currency) -> list:
        pass

    @abstractmethod
    def get_deposit_address(self, currency: Currency) -> str:
        pass

    @abstractmethod
    def get_all_deposit_addresses(self) -> dict:
        pass

    @abstractmethod
    def get_indirect_price(self, sold: Currency, sale: Currency, date_time: datetime = None):
        pass

    @abstractmethod
    def buy(self, sold: Currency, sale: Currency, quantity: Decimal, price: Optional[Decimal]) -> Order:
        pass

    @abstractmethod
    def sell(self, sold: Currency, sale: Currency, quantity: Decimal, price: Optional[Decimal]) -> Order:
        pass

    @abstractmethod
    def open_order(self, sold: Currency, sale: Currency, price: Decimal, quantity: Decimal, is_buy_type: bool) -> Order:
        pass

    @abstractmethod
    def cancel_order(self, uid):
        pass

    @abstractmethod
    def get_middle_currencies(self, currency_sold, currency_sale) -> tuple:
        pass

    @abstractmethod
    def withdraw(self, currency: Currency, count: Decimal, address: str) -> str:
        """

        :param currency: Currency of withdraw
        :param count: Count of withdraw
        :param address: External address for withdraw
        :return: External transaction id.
        """
        pass

    @abstractmethod
    def get_candles(self,
                    sold: Currency,
                    sale: Currency,
                    term: Union[Tuple[datetime, datetime], timedelta],
                    interval: timedelta) -> Tuple[Candle]:
        pass

    @abstractmethod
    def get_valid_intervals(self) -> Optional[Iterable[timedelta]]:
        pass
