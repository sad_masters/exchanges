import logging
from decimal import Decimal
from datetime import datetime

from ...objects import Currency
from ...services import ExchangeNotSupportAtDt, ExchangeNotSupport


def preprocess_get_price(method):
    def wrapper(self, sold: Currency, sale: Currency, date_time: datetime = None, *args, **kwargs):
        market = self._get_valid_market(sold, sale, is_check=False)

        if market.is_pseudo:
            return Decimal('1')

        if self.is_market_present(market, is_reverse=False):
            return method(self, sold, sale, date_time, *args, **kwargs)

        logging.info("Direct: can't find price %s with %s. Swap currencies." % (sold.name, sale.name))

        return Decimal(1) / method(self, sale, sold, date_time, *args, **kwargs)

    return wrapper


def preprocess_indirect_get_price(method):
    def wrapper(self, sold: Currency, sale: Currency, date_time: datetime = None):
        try:
            return method(self, sold, sale, date_time)

        except ExchangeNotSupportAtDt as exception:
            raise exception

        except ExchangeNotSupport:
            logging.info("Indirect: can't find price %s with %s. Swap currencies." % (sold.name, sale.name))
            return Decimal(1) / method(self, sale, sold, date_time)

    return wrapper


def preprocess_open_order(method):
    def wrapper(self, sold: Currency, sale: Currency, price: Decimal, quantity: Decimal, is_buy_type: bool):
        try:
            return method(self, sold, sale, price, quantity, is_buy_type)

        except ExchangeNotSupport:
            logging.info("Can't open order with currency sold: %s and sale: %s. Swap currencies." % (sold.name,
                                                                                                     sale.name))
            return method(self, sale, sold, price, quantity, is_buy_type)

    return wrapper
