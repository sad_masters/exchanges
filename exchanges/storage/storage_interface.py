from abc import abstractmethod

from .interfaces.keys_storage_interface import KeysStorageInterface
from .interfaces.cash_storage_interface import CashStorageInterface


class StorageInterface(CashStorageInterface, KeysStorageInterface):
    @abstractmethod
    def get_currencies(self) -> dict:
        pass

    @abstractmethod
    def insert_currency(self, title: str, ticker: str) -> bool:
        pass

    @abstractmethod
    def insert_key(self, secret: str, api: str, platform, user: str) -> bool:
        pass

    @abstractmethod
    def get_keys(self, platform, user: str) -> tuple:
        pass

    @abstractmethod
    def remove_key(self, platform, user: str) -> bool:
        pass