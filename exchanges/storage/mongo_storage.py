from typing import Optional

import pymongo as db
from pymongo.collection import Collection
from pymongo.database import Database
from pymongo.errors import ServerSelectionTimeoutError, OperationFailure

from .pure_platforms import PurePlatforms

from .interfaces.cash_storage_interface import CashStorageInterface
from .interfaces.keys_storage_interface import KeysStorageInterface


class MongoStorage(CashStorageInterface, KeysStorageInterface):

    def __init__(self, host: str, port: int, database_name: str, user: str = None, pwd: str = None):
        if user is None or len(user) == 0:
            uri = f"mongodb://{host}:{port}"
        else:
            uri = f"mongodb://{user}:{pwd}@{host}:{port}/?authSource={'admin'}&authMechanism=SCRAM-SHA-1"

        self._client = db.MongoClient(uri)

        self._db: Database = self._client[database_name]

        try:
            self._client.server_info()

        except ServerSelectionTimeoutError as err:
            raise AssertionError("Can't connect to database with error %s. Fix config file. %s:%s dbname - %s" %
                                 (str(err), host, port, database_name))

        except OperationFailure as err:
            raise AssertionError("Can't log in database with error %s. Fix config file. %s:%s dbname - %s. user: %s" %
                                 (str(err), host, port, database_name, user))

    @staticmethod
    def from_configuration(configuration=None):
        if configuration is None:
            from exchanges.configuration import Mongo as configuration

        return MongoStorage(host=configuration.Host,
                            port=configuration.Port,
                            database_name=configuration.DatabaseName,
                            user=configuration.User,
                            pwd=configuration.Password)

    def get_currencies(self) -> dict:
        return {str(cur['ticker']): str(cur["title"]) for cur in self._currencies.find() if
                not str(cur['ticker']).isdigit()}

    def insert_currency(self, title: str, ticker: str):
        self._currencies.replace_one(dict(ticker=ticker), dict(ticker=ticker, title=title), upsert=True)

    def clarify_currency_title(self, ticker, title):
        self._currencies.replace_one(dict(ticker=ticker), dict(ticker=ticker, title=title))

    def get_keys(self, platform: PurePlatforms, user: str) -> tuple:
        keys = self._keys.find_one(dict(platform=self._parse_platform(platform)))

        if keys is None:
            raise ValueError("Can't find keys for %s." % platform.name)

        return keys['api'], keys['secret']

    def insert_key(self, secret: str, api: str, platform, user: str = None):
        platform = self._parse_platform(platform)

        self._db.keys.replace_one(filter=dict(platform=platform),
                                  replacement=dict(platform=platform, api=api, secret=secret),
                                  upsert=True)

    @staticmethod
    def _parse_platform(platform):
        from exchanges.objects.platforms import Platforms

        if isinstance(platform, Platforms):
            platform = platform.name

        return platform

    def insert_data(self, platform, hash_: float, data: dict):
        self._cash.replace_one(filter=dict(platform=platform, hash=hash_),
                               replacement=dict(platform=platform, hash_=hash_, data=data),
                               upsert=True)

    def get_data(self, platform, hash_: float) -> Optional[dict]:
        result = self._cash.find_one(dict(platform=platform, hash=hash_))
        if result is None:
            return None

        if 'data' not in result:
            raise ValueError("Find data in Mongo, but can't take cash data")

        return result['data']

    def remove_key(self, platform, user: str) -> bool:
        raise NotImplemented()

    @property
    def _keys(self) -> Collection:
        return self._db.keys

    @property
    def _currencies(self) -> Collection:
        return self._db.currencies

    @property
    def _cash(self) -> Collection:
        return self._db.cash
