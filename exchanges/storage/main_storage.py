from typing import Union, Tuple, Optional, Type

from ..configuration import Assemble
from ..services.singleton import Singleton
from .interfaces.cash_storage_interface import CashStorageInterface
from .interfaces.keys_storage_interface import KeysStorageInterface


def _switch_storage_handler(handler: str) -> Union[Type[CashStorageInterface], Type[KeysStorageInterface]]:
    if handler == "MongoStorage":
        from .mongo_storage import MongoStorage
        return MongoStorage

    if handler == 'FileStorage':
        from .file_storage import FileStorage
        return FileStorage

    if handler == 'KeePassStorage':
        from .kee_pass_storage import KeePassStorage
        return KeePassStorage

    raise ValueError(f"Can't find storage handler {handler}")


def _get_configuration(configuration: Assemble):
    cash_handler: Type[CashStorageInterface] = _switch_storage_handler(configuration.CashHandlers)
    keys_handler: Type[KeysStorageInterface] = _switch_storage_handler(configuration.KeysHandler)

    return cash_handler.from_configuration(configuration.Cash), \
           keys_handler.from_configuration(configuration.Keys)


def get_test_configuration() -> Tuple[CashStorageInterface, KeysStorageInterface]:
    from ..configuration import Test
    return _get_configuration(Test)


def get_release_configuration() -> Tuple[CashStorageInterface, KeysStorageInterface]:
    from ..configuration import Release
    return _get_configuration(Release)


class MainStorage(CashStorageInterface, KeysStorageInterface, metaclass=Singleton):
    def __init__(self,
                 cash_storage: CashStorageInterface = None,
                 keys_storage: KeysStorageInterface = None,
                 is_production: Optional[bool] = None):

        if cash_storage is None and keys_storage is None:
            if is_production:
                self._cash_storage, self._keys_storage = get_release_configuration()
            else:
                self._cash_storage, self._keys_storage = get_test_configuration()

        elif is_production is not None:
            raise ValueError("Can't initialize from configuration + from arguments. Choose one type")

        elif None in (cash_storage, keys_storage):
            raise ValueError('Initialize both storage interfaces')

        else:
            self._cash_storage, self._keys_storage = cash_storage, keys_storage

        self.set_storage(self._cash_storage, CashStorageInterface)
        self.set_storage(self._keys_storage, KeysStorageInterface)

    @staticmethod
    def get_local():
        from exchanges.configuration import AppDataStorage
        from exchanges.storage.file_storage import FileStorage
        return FileStorage(AppDataStorage.Cash, AppDataStorage.Currencies)

    def set_storage(self, instance: Union[KeysStorageInterface, CashStorageInterface], storage_type: type):

        for method_name in filter(lambda m: not m.startswith("__") and callable(getattr(storage_type, m)),
                                  dir(storage_type)):

            method = getattr(instance, method_name)

            if method is None:
                raise ValueError(
                    f"Can't set main storage to {storage_type}: {instance}. Can't find {method_name} method.")

            setattr(self, method_name, method)

    def get_cash_storage(self):
        return self._cash_storage

    def get_keys_storage(self):
        return self._keys_storage
