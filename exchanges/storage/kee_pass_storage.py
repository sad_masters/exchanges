from pathlib import Path
from typing import Union, Optional

from enum import Enum

from .interfaces.keys_storage_interface import KeysStorageInterface
from .pure_platforms import PurePlatforms

from pykeepass import PyKeePass as KeePass
from pykeepass.group import Group


class KeePassStorage(KeysStorageInterface):
    def __init__(self, filename: Union[str, Path], password: str, key_file: Union[str, None, Path], keys_group: str):

        if isinstance(filename, Path):
            if not filename.exists():
                raise ValueError(f"Can't find kee pass file for {filename}")

            filename = filename.absolute()

        if isinstance(key_file, Path):
            if not key_file.exists():
                raise ValueError(f"Can't find key file {str(key_file.absolute())}")

            key_file = key_file.absolute()

        self._kp: KeePass = KeePass(filename=filename, password=password, keyfile=key_file)
        self._default_group: Group = self._kp.find_groups(name=keys_group, first=True)

    @staticmethod
    def from_configuration(config=None):
        import exchanges.exchanges.configuration as default
        config = config or default
        config: config.KeePass = getattr(config, 'KeePass', config)

        if config is None or getattr(config, "Path", None) is None:
            raise ValueError(f"Can't get from configuration {config}: KeePass storage params.")

        return KeePassStorage(filename=config.Path,
                              password=config.MasterPassword,
                              key_file=config.KeyFile,
                              keys_group=config.DefaultGroup)

    def _get_group(self, user: Optional[str]) -> Group:
        if user is not None:
            return self._kp.find_groups(name=user, first=True)
        return self._default_group

    def insert_key(self, api: str, secret: str, platform: Union[PurePlatforms, str], user: Optional[str] = None) -> bool:

        self.remove_key(platform, user)

        if isinstance(platform, Enum):
            platform = platform.name

        elif isinstance(platform, str) and platform not in PurePlatforms.names():
            raise ValueError(f"Can't find platform {platform}")

        self._kp.add_entry(destination_group=self._get_group(user),
                           title=platform,
                           username=api,
                           password=secret)
        self._kp.save()
        return True

    def get_keys(self, platform: Union[PurePlatforms, str], user: str = None) -> tuple:
        if isinstance(platform, Enum):
            platform = platform.name

        elif isinstance(platform, str) and platform not in PurePlatforms.names():
            raise ValueError(f"Can't insert key for {platform}. Can't find platform.")

        for entrie in self._kp.find_entries(group=self._get_group(user), title=platform):
            return entrie.username, entrie.password

        return None, None

    def remove_key(self, platform: Union[PurePlatforms, str], user: Optional[str]) -> bool:

        entries = self._kp.find_entries(destination_group=self._get_group(user), title=platform.name, notes=user)

        if entries:
            entries.delete()
            self._kp.save()
            return True

        return False
