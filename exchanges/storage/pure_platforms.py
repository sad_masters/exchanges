from enum import Enum


class PurePlatforms(Enum):
    Bittrex = 0
    Binance = 1
    Poloniex = 2
    Idex = 3
