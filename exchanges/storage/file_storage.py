import json
from abc import ABC
from json import JSONDecodeError
from pathlib import Path
from typing import Optional

from .interfaces.cash_storage_interface import CashStorageInterface


class FileStorage(CashStorageInterface, ABC):
    def __init__(self, cash_file: Path, currencies_file: Optional[Path]):
        self._path: Path = cash_file
        self._currencies_path: Path = currencies_file

        try:
            with open(str(cash_file), 'r') as fp:
                self.cash = json.load(fp=fp)
        except JSONDecodeError:
            self.cash = {}

        if self._currencies_path:

            try:
                with open(str(self._currencies_path), 'r') as fp:
                    self.cash['currencies'] = json.load(fp=fp)
            except JSONDecodeError:
                self.cash['currencies'] = {}

        else:
            self.cash['currencies'] = self.cash.get('currencies', {})

    def store(self):
        cash = getattr(self, 'cash', {})
        if cash is None:
            raise ValueError()

        with open(str(self._path), 'w') as fp:
            json.dump(cash, fp=fp)

        if self._currencies_path:
            with open(str(self._currencies_path), 'w') as fp:
                json.dump(self.cash['currencies'], fp)

    @staticmethod
    def from_configuration(configuration=None) -> 'FileStorage':
        if configuration is None:
            from exchanges.configuration import AppDataStorage
            configuration = AppDataStorage

        return FileStorage(configuration.Cash, configuration.Currencies)

    @classmethod
    def get_currencies_local_storage(cls) -> 'FileStorage':
        return cls.from_configuration()

    def get_currencies(self) -> dict:
        return self.cash.get('currencies', {})

    def insert_currency(self, title: str, ticker: str) -> bool:
        self.cash['currencies'][ticker] = title
        self.store()
        return True

    def insert_data(self, platform, hash_: float, data: dict):
        try:
            self.cash[platform][hash_] = data
        except Exception:
            self.cash[platform] = dict()
            self.cash[platform][hash_] = data
        self.store()

    def get_data(self, platform, hash_: float) -> dict:
        try:
            return self.cash[platform][hash_]

        except IndexError:
            return {}
        except KeyError:
            return {}
