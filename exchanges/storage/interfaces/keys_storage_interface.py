from abc import abstractmethod


class KeysStorageInterface:

    @staticmethod
    def from_configuration(configuration) -> 'KeysStorageInterface':
        pass

    @abstractmethod
    def insert_key(self, secret: str, api: str, platform, user: str) -> bool:
        pass

    @abstractmethod
    def get_keys(self, platform, user: str) -> tuple:
        pass

    @abstractmethod
    def remove_key(self, platform, user: str) -> bool:
        pass
