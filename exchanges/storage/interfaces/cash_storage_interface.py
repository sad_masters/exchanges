from abc import abstractmethod


class CashStorageInterface:

    @staticmethod
    def from_configuration(configuration) -> 'CashStorageInterface':
        pass

    @abstractmethod
    def get_currencies(self) -> dict:
        pass

    @abstractmethod
    def insert_currency(self, title: str, ticker: str) -> bool:
        pass

    @abstractmethod
    def insert_data(self, platform, hash_: float, data: dict):
        pass

    @abstractmethod
    def get_data(self, platform, hash_: float) -> dict:
        pass
