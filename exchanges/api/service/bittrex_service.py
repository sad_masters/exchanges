import getpass
import json
import requests
from enum import Enum

from cryptography.hazmat.primitives.ciphers.algorithms import AES

encrypted = False


class BittrexConstants(Enum):
    BUY_ORDERBOOK = 'buy'
    SELL_ORDERBOOK = 'sell'
    BOTH_ORDERBOOK = 'both'

    TRADE_FEE = 0.0025

    TICKINTERVAL_ONEMIN = 'oneMin'
    TICKINTERVAL_FIVEMIN = 'fiveMin'
    TICKINTERVAL_HOUR = 'hour'
    TICKINTERVAL_THIRTYMIN = 'thirtyMin'
    TICKINTERVAL_DAY = 'Day'

    ORDERTYPE_LIMIT = 'LIMIT'
    ORDERTYPE_MARKET = 'MARKET'

    TIMEINEFFECT_GOOD_TIL_CANCELLED = 'GOOD_TIL_CANCELLED'
    TIMEINEFFECT_IMMEDIATE_OR_CANCEL = 'IMMEDIATE_OR_CANCEL'
    TIMEINEFFECT_FILL_OR_KILL = 'FILL_OR_KILL'

    CONDITIONTYPE_NONE = 'NONE'
    CONDITIONTYPE_GREATER_THAN = 'GREATER_THAN'
    CONDITIONTYPE_LESS_THAN = 'LESS_THAN'
    CONDITIONTYPE_STOP_LOSS_FIXED = 'STOP_LOSS_FIXED'
    CONDITIONTYPE_STOP_LOSS_PERCENTAGE = 'STOP_LOSS_PERCENTAGE'

    API_V1_1 = 'v1.1'
    API_V2_0 = 'v2.0'

    BASE_URL_V1_1 = 'https://bittrex.com/api/v1.1{path}?'
    BASE_URL_V2_0 = 'https://bittrex.com/api/v2.0{path}?'

    PROTECTION_PUB = 'pub'  # public methods
    PROTECTION_PRV = 'prv'  # authenticated methods


def encrypt(api_key, api_secret, export=True, export_fn='secrets.json'):
    """
    Function to encrypt data.
    :param api_key:
    :param api_secret:
    :param export:
    :param export_fn:
    :return: dict
    """

    cipher = AES.new(getpass.getpass(
        'Input encryption password (string will not show)'))
    api_key_n = cipher.encrypt(api_key)
    api_secret_n = cipher.encrypt(api_secret)
    api = {'key': str(api_key_n), 'secret': str(api_secret_n)}
    if export:
        with open(export_fn, 'w') as outfile:
            json.dump(api, outfile)
    return api


def using_requests(request_url, apisign):
    """
    Function get request with sign.
    :param request_url:
    :param apisign:
    :return: JSON Format
    """
    return requests.get(
        request_url,
        headers={"apisign": apisign},
        timeout=10
    ).json()
