import hashlib
from copy import copy
from typing import Optional, Iterable
from urllib.parse import urlencode

from ..binance_api import BinanceAPI
from ...storage import CashStorageInterface


class CashBinanceAPI(BinanceAPI):
    hash_key_secret = None

    def __init__(self, api_key, api_secret, cash_interface: CashStorageInterface, requests_params=None):
        hsh = hashlib.sha256()
        hsh.update((api_key + api_secret).encode('utf-8'))
        self.hash_key_secret = hsh.hexdigest()
        self._cash = cash_interface

        super(CashBinanceAPI, self).__init__(api_key, api_secret, requests_params)

    @property
    def cached_methods(self) -> Iterable[str]:
        return 'openOrders', 'ticker/allBookTickers', 'myTrades', 'depositAddress.html'

    def _request(self, method, uri, signed, force_params=False, **kwargs):
        if isinstance(self.cached_methods, type('string')):
            if self.cached_methods not in uri:
                return super()._request(method, uri, signed, force_params, **kwargs)
        else:
            is_cashed = False
            for cash_method in self.cached_methods:
                if cash_method in uri:
                    is_cashed = True
            if not is_cashed:
                return super()._request(method, uri, signed, force_params, **kwargs)

        if kwargs.get('data', None):
            cash_options = copy(kwargs['data'])
            cash_options.update({'method': method, 'uri': uri})
        else:
            cash_options = {'method': method, 'uri': uri}

        data = self._get_from_storage(cash_options)

        if len(data) == 0:
            data = super()._request(method, uri, signed, force_params, **kwargs)
            if data:
                self._insert_to_hash(cash_options, data)

        return data

    def _get_hash(self, options) -> float:
        hsh = hashlib.sha256()
        hsh.update(urlencode({key: options[key] for key in sorted(options.keys())}).encode('utf-8'))

        return hsh.hexdigest() + self.hash_key_secret

    def _get_from_storage(self, options: dict) -> Optional[dict]:
        return self._cash.get_data('Binance', self._get_hash(options))

    def _insert_to_hash(self, options: dict, data: dict):
        return self._cash.insert_data('Binance', self._get_hash(options), data)
