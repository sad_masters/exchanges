import hashlib
from copy import copy
from typing import Optional, Iterable
from urllib.parse import urlencode

from ..bittrex_api import BittrexAPI
from ..service.bittrex_service import using_requests, BittrexConstants
from ...storage import CashStorageInterface


class CashBittrexAPI(BittrexAPI):
    hash_key_secret = None

    def __init__(self, api_key, api_secret, cash_interface: CashStorageInterface,
                 calls_per_second=1,
                 dispatch=using_requests,
                 api_version=BittrexConstants.API_V1_1.value):
        hsh = hashlib.sha256()
        hsh.update((api_key + api_secret).encode('utf-8'))
        self.hash_key_secret = hsh.hexdigest()
        self._cash = cash_interface

        super(CashBittrexAPI, self).__init__(api_key, api_secret, calls_per_second, dispatch, api_version)

    @property
    def cached_methods(self) -> Iterable[str]:
        return '/public/getmarkets', '/public/getmarkethistory'

    def _api_query(self, protection=None, path_dict=None, options=None):
        if path_dict[self.api_version] not in self.cached_methods:
            return super()._api_query(protection, path_dict, options)

        if options:
            cash_options = copy(options)
            cash_options.update({self.api_version: path_dict[self.api_version]})
        else:
            cash_options = {self.api_version: path_dict[self.api_version]}

        data = self._get_from_storage(cash_options)

        if not data:
            data = super()._api_query(protection, path_dict, options)
            if data['success']:
                self._insert_to_hash(cash_options, data)

        return data

    def _get_hash(self, options) -> float:
        hsh = hashlib.sha256()
        hsh.update(urlencode({key: options[key] for key in sorted(options.keys())}).encode('utf-8'))

        return hsh.hexdigest() + self.hash_key_secret

    def _get_from_storage(self, options: dict) -> Optional[dict]:
        return self._cash.get_data('Bittrex', self._get_hash(options))

    def _insert_to_hash(self, options: dict, data: dict):
        return self._cash.insert_data('Bittrex', self._get_hash(options), data)
