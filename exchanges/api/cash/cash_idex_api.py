import hashlib
from copy import copy
from typing import Optional, Iterable
from urllib.parse import urlencode

from ..idex_api import IdexAPI
from ...storage import CashStorageInterface


class CashIdexAPI(IdexAPI):
    hash_key_secret = None

    def __init__(self, cash_interface: CashStorageInterface, private_key=None, address=None):
        hsh = hashlib.sha256()
        hsh.update((private_key + address).encode('utf-8'))
        self.hash_key_secret = hsh.hexdigest()
        self._cash = cash_interface

        super(CashIdexAPI, self).__init__(private_key=private_key, address=address)

    @property
    def cached_methods(self) -> Iterable[str]:
        return 'returnTicker', 'returnTradeHistory'

    def _request(self, method, path, signed, **kwargs):
        if path not in self.cached_methods:
            return super()._request(method, path, signed, **kwargs)

        if kwargs.get('json', None):
            cash_options = copy(kwargs['json'])
            cash_options.update({'method': method, 'path': path})
        else:
            cash_options = {'method': method, 'path': path}

        data = self._get_from_storage(cash_options)

        if data is None or len(data) == 0:
            data = super()._request(method, path, signed, **kwargs)
            if data:
                self._insert_to_hash(cash_options, data)

        return data

    def _get_hash(self, options) -> float:
        hsh = hashlib.sha256()
        hsh.update(urlencode({key: options[key] for key in sorted(options.keys())}).encode('utf-8'))

        return hsh.hexdigest() + self.hash_key_secret

    def _get_from_storage(self, options: dict) -> Optional[dict]:
        return self._cash.get_data('Idex', self._get_hash(options))

    def _insert_to_hash(self, options: dict, data: dict):
        return self._cash.insert_data('Idex', self._get_hash(options), data)
