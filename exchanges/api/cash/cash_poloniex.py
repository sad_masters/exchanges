import hashlib
from copy import copy
from typing import Optional, Iterable
from urllib.parse import urlencode

from ..poloniex_api import PoloniexAPI
from ...storage import CashStorageInterface


class CashPoloniexAPI(PoloniexAPI):
    hash_key_secret = None

    def __init__(self, api_key, api_secret, cash_interface: CashStorageInterface):
        hsh = hashlib.sha256()
        hsh.update((api_key + api_secret).encode('utf-8'))
        self.hash_key_secret = hsh.hexdigest()
        self._cash = cash_interface

        super(CashPoloniexAPI, self).__init__(api_key, api_secret)

    @property
    def cached_methods(self) -> Iterable[str]:
        return 'returnTicker', 'returnDepositAddresses', 'returnTradeHistory'

    def api_query(self, options):
        if options['command'] not in self.cached_methods:
            return super().api_query(options)

        data = self._get_from_storage(options)

        if data is None or len(data) == 0:
            data = super().api_query(copy(options))
            if data:
                self._insert_to_hash(options, data)

        return data

    def _get_hash(self, options) -> float:
        hsh = hashlib.sha256()
        hsh.update(urlencode({key: options[key] for key in sorted(options.keys())}).encode('utf-8'))

        return hsh.hexdigest() + self.hash_key_secret

    def _get_from_storage(self, options: dict) -> Optional[dict]:
        return self._cash.get_data('Poloniex', self._get_hash(options))

    def _insert_to_hash(self, options: dict, data: dict):
        return self._cash.insert_data('Poloniex', self._get_hash(options), data)
