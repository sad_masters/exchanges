import time
import json
import urllib
import http.client
import hmac
import hashlib
from urllib.parse import urlparse, urlencode


class PoloniexAPI:

    def __init__(self, api_key, api_secret):
        self.API_KEY = api_key
        self.API_SECRET = bytearray(api_secret, encoding='utf-8')

    def api_query(self, options):
        """
        Main Poloniex api function
        :param options: dict
        :return: dict
        """
        request_url = 'https://poloniex.com/' + options['method']

        if 'currencyPair' in options:
            market = options['currencyPair']
            index = market.find('_')
            options['currencyPair'] = "{}_{}".format(market[index+1:], market[:index])

        if options['method'] == 'public':
            options.pop('method')
            request_url += '?' + urlencode(options)
            http_method = "GET"
        else:
            options.pop('method')
            http_method = "POST"

        time.sleep(0.2)
        payload = {'nonce': int(round(time.time() * 1000))}

        if options:
            payload.update(options)

        payload = urllib.parse.urlencode(payload)

        H = hmac.new(key=self.API_SECRET, digestmod=hashlib.sha512)
        H.update(payload.encode('utf-8'))
        sign = H.hexdigest()

        headers = {"Content-type": "application/x-www-form-urlencoded",
                   "Key": self.API_KEY,
                   "Sign": sign}

        url_o = urlparse(request_url)
        conn = http.client.HTTPSConnection(url_o.netloc)
        conn.request(method=http_method, url=request_url, body=payload, headers=headers)
        response_get = conn.getresponse()
        response = response_get.read()

        try:
            obj = json.loads(response.decode('utf-8'))

            if 'error' in obj and obj['error']:
                raise Exception(obj['error'])
            return obj
        except ValueError:
            raise Exception(
                'Incorrect Data (verify name of the api method {api_method})'.format(
                    api_method=options['command']))
        finally:
            conn.close()

    def returnTicker(self):
        """
        Returns the ticker for all markets.
        :return: dict
        """
        return self.api_query({'method': 'public', 'command': 'returnTicker'})

    def return24hVolume(self):
        """
        Returns the 24-hour volume for all markets, plus totals for primary currencies.
        :return: dict
        """
        return self.api_query({'method': 'public', 'command': 'return24hVolume'})

    def returnOrderBook(self, currencyPair, depth=None):
        """
        Returns the order book for a given market, as well as a sequence number for use with
        the Push api and an indicator specifying whether the market is frozen.
        You may set currencyPair to "all" to get the order books of all markets.
        :param currencyPair:
        :param depth:
        :return: dict
        """
        if depth is None:
            return self.api_query(
                {'command': 'returnOrderBook', 'method': 'public', 'currencyPair': currencyPair})
        else:
            return self.api_query({'command': 'returnOrderBook', 'method': 'public', 'currencyPair': currencyPair,
                                   'depth': depth})

    def publicReturnTradeHistory(self, currencyPair, start=None, end=None):
        """
        Returns the past 200 trades for a given market, or up to 50,000 trades
        between a range specified in UNIX timestamps by the "start" and "end" GET parameters.
        :param currencyPair:
        :param start:
        :param end:
        :return:
        """
        _dictionary = {'method': 'public', 'command': 'returnTradeHistory', 'currencyPair': currencyPair}

        if start is not None and end is not None:
            _dictionary.update({'start': start, 'end': end})

        return self.api_query(_dictionary)

    def returnChartData(self, currencyPair, start, end, period):
        """
        Returns candlestick chart data. Required GET parameters are "currencyPair",
        "period" (candlestick period in seconds; valid values are
        300, 900, 1800, 7200, 14400, and 86400), "start", and "end".
        "Start" and "end" are given in UNIX timestamp format and used to specify the date
        range for the data returned.
        :param currencyPair:
        :param start:
        :param end:
        :param period:
        :return:
        """
        return self.api_query({'method': 'public', 'command': 'returnChartData', 'currencyPair': currencyPair,
                               'start': start, 'end': end, 'period': period})

    def returnCurrencies(self):
        """
        Returns information about currencies.
        :return:
        """
        return self.api_query({'method': 'public', 'command': 'returnCurrencies'})

    def returnLoanOrders(self, currency):
        """
        Returns the list of loan offers and demands for a given currency, specified by the "currency" GET parameter.
        :param currency:
        :return:
        """
        return self.api_query({'method': 'public', 'command': 'returnLoanOrders', 'currency': currency})

    def returnBalances(self):
        """
        Returns all of your available balances.
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'returnBalances'})

    def returnCompleteBalances(self, account=None):
        """
        Returns all of your balances, including available balance, balance on orders,
        and the estimated BTC value of your balance. By default, this call is limited to your exchange account;
        set the "account" POST parameter to "all" to include your margin and lending accounts.
        :param account:
        :return:
        """
        _dictionary = {'method': 'tradingApi', 'command': 'returnCompleteBalances'}

        if account is None:
            _dictionary.update({'account': account})

        return self.api_query(_dictionary)

    def returnDepositAddresses(self):
        """
        Returns all of your deposit addresses.
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'returnDepositAddresses'})

    def generateNewAddress(self, currency):
        """
        Generates a new deposit address for the currency specified by the "currency" POST parameter.

        URGENT!
        Only one address per currency per day may be generated,
        and a new address may not be generated before the previously-generated one has been used.
        :param currency:
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'generateNewAddress', 'currency': currency})

    def returnDepositsWithdrawals(self, start, end):
        """
        Returns your deposit and withdrawal history within a range, specified by the "start" and "end" POST parameters,
        both of which should be given as UNIX timestamps.
        :param start:
        :param end:
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'returnDepositsWithdrawals', 'start': start, 'end': end})

    def returnOpenOrders(self, currencyPair):
        """
        Returns your open orders for a given market, specified by the "currencyPair" POST parameter, e.g. "BTC_XCP".
        Set "currencyPair" to "all" to return open orders for all markets.
        :param currencyPair:
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'returnOpenOrders', 'currencyPair': currencyPair})

    def tradeReturnTradeHistory(self, currencyPair, start=None, end=None, limit=None):
        """
        Returns your trade history for a given market, specified by the "currencyPair" POST parameter.
        You may specify "all" as the currencyPair to receive your trade history for all markets.
        You may optionally specify a range via "start" and/or "end" POST parameters, given in UNIX timestamp format;
        if you do not specify a range, it will be limited to one day.
        You may optionally limit the number of entries returned using the "limit" parameter, up to a maximum of 10,000.
        If the "limit" parameter is not specified, no more than 500 entries will be returned.
        :param currencyPair:
        :param start:
        :param end:
        :param limit:
        :return:
        """
        _dictionary = {'method': 'tradingApi', 'command': 'returnTradeHistory', 'currencyPair': currencyPair}

        if limit is not None:
            _dictionary.update({'limit': limit})

        if start is not None and end is not None:
            _dictionary.update({'start': start, 'end': end})

        return self.api_query(_dictionary)

    def returnOrderTrades(self, orderNumber):
        """
        Returns all trades involving a given order, specified by the "orderNumber" POST parameter.
        If no trades for the order have occurred or you specify an order that does not belong to you, you will receive an error.
        :param orderNumber:
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'returnOrderTrades', 'orderNumber': orderNumber})

    def buy(self, currencyPair, rate, amount, fillOrKill=None, immediateOrCancel=None, postOnly=None):
        """
        Places a limit buy order in a given market. Required POST parameters are "currencyPair", "rate", and "amount".
        If successful, the method will return the order number.

        URGENT!
        You may optionally set "fillOrKill", "immediateOrCancel", "postOnly" to 1.
        A fill-or-kill order will either fill in its entirety or be completely aborted.
        An immediate-or-cancel order can be partially or completely filled,
        but any portion of the order that cannot be filled immediately will be canceled rather than left on the order book.
        A post-only order will only be placed if no portion of it fills immediately; this guarantees you will never pay
        the taker fee on any part of the order that fills.
        :param currencyPair:
        :param rate:
        :param amount:
        :param fillOrKill:
        :param immediateOrCancel:
        :param postOnly:
        :return:
        """
        _dictionary = {'method': 'tradingApi', 'command': 'buy', 'currencyPair': currencyPair, 'rate': rate,
                       'amount': amount}

        if fillOrKill is not None:
            _dictionary.update({'fillOrKill': fillOrKill})

        if immediateOrCancel is not None:
            _dictionary.update({'immediateOrCancel': immediateOrCancel})

        if postOnly is not None:
            _dictionary.update({'postOnly': postOnly})

        return self.api_query(_dictionary)

    def sell(self, currencyPair, rate, amount, fillOrKill=None, immediateOrCancel=None, postOnly=None):
        """
        Places a sell order in a given market.
        :param currencyPair:
        :param rate:
        :param amount:
        :param fillOrKill:
        :param immediateOrCancel:
        :param postOnly:
        :return:
        """
        _dictionary = {'method': 'tradingApi', 'command': 'sell', 'currencyPair': currencyPair, 'rate': rate,
                       'amount': amount}

        if fillOrKill is not None:
            _dictionary.update({'fillOrKill': fillOrKill})

        if immediateOrCancel is not None:
            _dictionary.update({'immediateOrCancel': immediateOrCancel})

        if postOnly is not None:
            _dictionary.update({'postOnly': postOnly})

        return self.api_query(_dictionary)

    def cancelOrder(self, orderNumber):
        """
        Cancels an order you have placed in a given market. Required POST parameter is "orderNumber".
        :param orderNumber:
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'cancelOrder', 'orderNumber': orderNumber})

    def moveOrder(self, orderName, rate, amount=None, postOnly=None, directOrCancel=None):
        """
        Cancels an order and places a new one of the same type in a single atomic transaction,
        meaning either both operations will succeed or both will fail.
        Required POST parameters are "orderNumber" and "rate";
        you may optionally specify "amount" if you wish to change the amount of the new order.
        "postOnly" or "immediateOrCancel" may be specified for exchange orders, but will have no effect on margin orders
        :param orderName:
        :param rate:
        :param amount:
        :param postOnly:
        :param directOrCancel:
        :return:
        """
        _dictionary = {'method': 'tradingApi', 'command': 'moveOrder', 'orderName': orderName, 'rate': rate}

        if amount is not None:
            _dictionary.update({'amount': amount})

        if postOnly is not None and directOrCancel is None:
            _dictionary.update({'postOnly': postOnly})

        if postOnly is None and directOrCancel is not None:
            _dictionary.update({'directOrCancel': directOrCancel})

        return self.api_query(_dictionary)

    def withdraw(self, currency, amount, address, paymentld=None):
        """
        Immediately places a withdrawal for a given currency, with no email confirmation. In order to use this method,
        the withdrawal privilege must be enabled for your api key. Required POST parameters are "currency", "amount",
        and "address". For XMR withdrawals, you may optionally specify "paymentId".
        :param currency:
        :param amount:
        :param address:
        :param paymentld:
        :return:
        """
        _dictionary = {'method': 'tradingApi', 'command': 'withdraw', 'currency': currency, 'amount': amount,
                       'address': address}

        if paymentld is not None:
            _dictionary.update({'paymentld': paymentld})

        return self.api_query(_dictionary)

    def returnFeeInfo(self):
        """
        If you are enrolled in the maker-taker fee schedule, returns your current trading fees and trailing 30-day
        volume in BTC. This information is updated once every 24 hours.
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'returnFeeInfo'})

    def returnAvailableAccountBalances(self, account=None):
        """
        Returns your balances sorted by account. You may optionally specify the "account" POST parameter if you wish to
        fetch only the balances of one account. Please note that balances in your margin account may not be accessible
        if you have any open margin positions or orders.
        :param account:
        :return:
        """
        _dictionary = {'method': 'tradingApi', 'command': 'returnAvailableAccountBalances'}

        if account is not None:
            _dictionary.update({'account': account})

        return self.api_query(_dictionary)

    def returnTradableBalances(self):
        """
        Returns your current tradable balances for each currency in each market for which margin trading is enabled.
        Please note that these balances may vary continually with market conditions.
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'returnTradableBalances'})

    def transferBalance(self, currency, amount, fromAccount, toAccount):
        """
        Transfers funds from one account to another (e.g. from your exchange account to your margin account).
        Required POST parameters are "currency", "amount", "fromAccount", and "toAccount".
        :param currency:
        :param amount:
        :param fromAccount:
        :param toAccount:
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'transferBalance', 'currency': currency, 'amount': amount, 'fromAccount': fromAccount, 'toAccount': toAccount})

    def returnMarginAccountSummary(self):
        """
        Returns a summary of your entire margin account. This is the same information you will find in the Margin
        Account section of the Margin Trading page, under the Markets list.
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'returnMarginAccountSummary'})

    def marginBuy(self, currencyPair, rate, amount, lendingRate=None):
        """
        Places a margin buy order in a given market. Required POST parameters are "currencyPair", "rate", and "amount".
        You may optionally specify a maximum lending rate using the "lendingRate" parameter. If successful, the method
        will return the order number and any trades immediately resulting from your order.
        :param currencyPair:
        :param rate:
        :param amount:
        :param lendingRate:
        :return:
        """
        _dictionary = {'method': 'tradingApi', 'command': 'marginBuy', 'currencyPair': currencyPair, 'rate': rate,
                               'amount': amount}

        if lendingRate is not None:
            _dictionary.update({'lendingRate': lendingRate})

        return self.api_query(_dictionary)

    def marginSell(self, currencyPair, rate, amount, lendingRate=None):
        """
        Places a margin sell order in a given market. Parameters and output are the same as for the marginBuy method.
        :param currencyPair:
        :param rate:
        :param amount:
        :param lendingRate:
        :return:
        """
        _dictionary = {'method': 'tradingApi', 'command': 'marginSell', 'currencyPair': currencyPair, 'rate': rate,
                       'amount': amount}

        if lendingRate is not None:
            _dictionary.update({'lendingRate': lendingRate})

        return self.api_query(_dictionary)

    def getMarginPosition(self, currencyPair):
        """
        Returns information about your margin position in a given market, specified by the "currencyPair" POST parameter.
         You may set "currencyPair" to "all" if you wish to fetch all of your margin positions at once.
         If you have no margin position in the specified market, "type" will be set to "none".
         "liquidationPrice" is an estimate, and does not necessarily represent the price at which an actual forced
         liquidation will occur. If you have no liquidation price, the value will be -1.
        :param currencyPair:
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'getMarginPosition', 'currencyPair': currencyPair})

    def closeMarginPosition(self, currencyPair):
        """
        Closes your margin position in a given market (specified by the "currencyPair" POST parameter) using a market
        order. This call will also return success if you do not have an open position in the specified market.
        :param currencyPair:
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'closeMarginPosition', 'currencyPair': currencyPair})

    def createLoanOffer(self, currency, amount, duration, autoRenew, lendingRate):
        """
        Creates a loan offer for a given currency. Required POST parameters are "currency", "amount", "duration",
        "autoRenew" (0 or 1), and "lendingRate".
        :param currency:
        :param amount:
        :param duration:
        :param autoRenew:
        :param lendingRate:
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'createLoanOffer', 'currency': currency, 'amount': amount, 'duration': duration, 'autoRenew': autoRenew, 'lendingRate': lendingRate})

    def cancelLoanOffer(self, orderNumber):
        """"
        Cancels a loan offer specified by the "orderNumber" POST parameter.
        :param orderNumber:
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'cancelLoanOffer', 'orderNumber': orderNumber})

    def returnOpenLoanOffers(self):
        """
        Returns your open loan offers for each currency.
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'returnOpenLoanOffers'})

    def returnActiveLoans(self):
        """
        Returns your active loans for each currency.
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'returnActiveLoans'})

    def returnLendingHistory(self, start, end, limit=None):
        """
        Returns your lending history within a time range specified by the "start" and "end" POST parameters as UNIX
        timestamps. "limit" may also be specified to limit the number of rows returned.
        :param start:
        :param end:
        :param limit:
        :return:
        """
        _dictionary = {'method': 'tradingApi', 'command': 'returnLendingHistory', 'start': start, 'end': end}

        if limit is not None:
            _dictionary.update({'limit': limit})

        return self.api_query(_dictionary)

    def toggleAutoRenew(self, orderNumber):
        """
        Toggles the autoRenew setting on an active loan, specified by the "orderNumber" POST parameter. If successful,
        "message" will indicate the new autoRenew setting.
        :param orderNumber:
        :return:
        """
        return self.api_query({'method': 'tradingApi', 'command': 'toggleAutoRenew', 'orderNumber': orderNumber})
