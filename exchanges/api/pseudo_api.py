import random
from collections import namedtuple
from datetime import datetime, timedelta
from decimal import Decimal
from random import randint, triangular, choices
from multiprocessing import Process

from typing import Generator, Dict, Optional, Tuple, Union

PseudoDeal = namedtuple("Deal", ('dt', 'price', 'volume'))


class PseusoApi:
    @staticmethod
    def _get_random_decimal(length, bit_capacity) -> Decimal:
        import string
        return Decimal(''.join(choices(population=tuple(string.digits), k=length))) / (10 ** (length - bit_capacity))

    @classmethod
    def generate_deals(cls,
                       current_price: Optional[Decimal],
                       term: Union[timedelta, Tuple[datetime, datetime]]) -> Generator[PseudoDeal, None, None]:
        if isinstance(term, timedelta):
            term = datetime.now() - term, datetime.now()

        current_price = current_price or cls._get_random_decimal(14, randint(1, 5))
        current = term[0]

        step = 0.003
        while current <= term[1]:
            dt, current = current, current + timedelta(seconds=randint(0, 1), microseconds=randint(0, 100))
            current_price = current_price * Decimal.from_float(triangular(1 - step, 1 + step, mode=1))

            yield PseudoDeal(dt=dt, price=current_price, volume=cls._get_random_decimal(10, randint(1, 3)))
