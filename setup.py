from datetime import datetime

from setuptools import setup, find_packages
import platform


_project_name = "exchanges"


if __name__ == '__main__':

    install_requires: list = ['py_ecc',
                              'eth_utils<2,>=1.2.0',
                              'requests',
                              'cryptography',
                              'pymongo',
                              'lxml',
                              'nose',
                              'dateparser',
                              'pytz',
                              'pykeepass']

    dependency_links: list = list()

    if platform.system() == 'Linux':
        install_requires.append('pyethash>=0.1.23')
        install_requires.append('ethereum>=2.3.1')
    else:
        install_requires.append('pyethash==0.1.23')
        install_requires.append('ethereum==2.3.1')
        dependency_links.append('git+https://github.com/VartanGurgen/ethash.git@master#egg=pyethash-0.1.23')
        dependency_links.append('git+https://github.com/VartanGurgen/pyethereum.git@master#egg=ethereum-2.3.1')

    setup(
        name=_project_name,
        version='0.1.1.%s' % (datetime.now() - datetime(year=2018, month=6, day=1)).seconds,
        packages=find_packages(),
        url='',
        license='',
        author=["vartangurgen", "q99"],
        author_email='',
        install_requires=install_requires,
        dependency_links=dependency_links
    )

    from exchanges.app_data_help import create_application_data_folder

    create_application_data_folder()
