from exchanges_tools import SimpleManager
from exchanges.storage.file_storage import FileStorage

if __name__ == '__main__':
    mng = SimpleManager()
    storage = FileStorage.get_currencies_local_storage()
    for currency in mng.update_currency():
        storage.insert_currency(ticker=currency.ticker, title=currency.title)
    storage.store()

