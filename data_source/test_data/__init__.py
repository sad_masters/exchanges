from pathlib import Path

currencies_path = Path.cwd() / "data_source" / "test_data" / "manager_test_currencies.json"
deposit_addresses_path = Path.cwd() / "data_source" / "test_data" / "deposit_addresses.json"
