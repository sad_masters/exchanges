class Glass:
    def __init__(self, exchange_interface, market):
        self.market = market
        self.exchange_interface = exchange_interface
        self.update_open_orders()


    def order_list_former(self, is_buy):
        orders = list(filter(lambda order: order.is_buy.__xor__(is_buy), self.open_orders))
        return orders.sort(key=lambda order: order.price)

    def update_open_orders(self):
        self.open_orders = self.exchange_interface.get_open_orders(self.market.base)
        self.buy_orders, self.sell_orders = self.order_list_former(True), self.order_list_former(False)
