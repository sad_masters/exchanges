from exchanges.objects.market import Market
from exchanges.objects.order import Order
from exchanges.objects.platforms import Platforms
from exchanges.objects.currency import Currency
from exchanges_tools.simple_manager import SimpleManager
from exchanges.objects.glass import Glass
from exchanges_tools.visualizer_glass import VisualizerGlassDash as vgDash

import json
import time


class VisualizerTests():

    def __init__(self, base: Currency=Currency.BTC, market: Currency=Currency.USDT, delimeter: str='_',
                 request_count: int=10, interval: float=None, time_interval: float=0.7*1000):
        self.filename = 'JsonTestData.json'
        self.request_index = 0
        self.mkt = Market(base, market, delimeter)
        self.request_count = request_count
        self.interval = interval
        self.time_interval = time_interval
        self.test_request_cnt = 0
        self.start_time = None

    def update_json_file(self):
        """
        Write orders in json file
        """
        mng = SimpleManager()
        mng.update_currency()
        provider = mng.get_provider(Platforms.Binance)
        glass_order_list = []
        order_glass = Glass(provider.get_order_book, self.mkt)
        try:
            request_count = int(input('Please, enter request count to update test data file: '))
        except ValueError:
            print('input request count is not correct')
            request_count = 10
        for i in range(request_count):
            order_glass.update_orders()
            buy_orders = [{'price': order.price, 'quantity': order.quantity} for order in order_glass.buy_orders]
            sell_orders = [{'price': order.price, 'quantity': order.quantity} for order in order_glass.sell_orders]
            glass_order_list.append({'buy': buy_orders, 'sell': sell_orders})
            print('-')
            time.sleep(0.5)
        fout = open(self.filename, 'w')
        json.dump(glass_order_list, fout)
        fout.close()

    @staticmethod
    def order_from_dict(mkt: Market, orders: dict):
        """
        Return list of orders from json dict
        :param mkt:
        :param orders: dictionary with buy and sell order lists
        :return: List[Order]
        """
        buy_orders = list(map(lambda order: Order(mkt, order['price'],
                                                  order['quantity'], True),
                              orders['buy']))
        sell_orders = list(map(lambda order: Order(mkt, order['price'],
                                                   order['quantity'], False),
                               orders['sell']))
        return buy_orders + sell_orders

    def request_tester(self):
        """
        Print time to display request_count requests
        """
        if self.test_request_cnt == self.request_count:
            print(f'time to make {self.test_request_cnt} request = {time.time() - self.start_time} sec.')
            self.test_request_cnt = 0
        if self.test_request_cnt == 0:
            self.start_time = time.time()
        self.test_request_cnt += 1

    def orders_former(self, sold=None, sale=None):
        """
        Orders getter to use as getter in visualizer
        :param sold: fiction variable
        :param sale: fiction variable
        :return: List[Order]
        """
        try:
            fin = open(self.filename, 'r')
        except FileNotFoundError:
            print('This file does not exist. Now you need to wait a few minutes before creating this file.')
            self.update_json_file()
            fin = open(self.filename, 'r')
        json_file = json.load(fin)
        json_orders = json_file[self.request_index]
        self.request_index = (self.request_index + 1) % len(json_file)
        fin.close()
        orders = self.order_from_dict(self.mkt, json_orders)
        orders.sort(key=lambda order: order.price)
        self.request_tester()
        return orders

    def test_visualizer_run(self):
        test_glass = Glass(self.orders_former, self.mkt)
        test_visualizer = vgDash(test_glass, self.interval, self.time_interval)
        test_visualizer.run_server()


if __name__ == '__main__':
    test = VisualizerTests(interval=0.1)
    test.test_visualizer_run()
