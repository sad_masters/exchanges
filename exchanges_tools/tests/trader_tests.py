import unittest
import datetime

from trader.trader import Trader
from simple_manager import SimpleManager
from exchanges.providers import Currency, datetime, timedelta, Decimal, \
    Platforms


class TraderTests(unittest.TestCase):
    def __init__(self, methodName='runTest'):
        self.mng = SimpleManager()
        self.trd = Trader(self.mng.get_provider(Platforms.Idex))
        self.mkt = (Currency.HOT, Currency.ETH)
        self.end_term = datetime(2018, 8, 20, 16, 30, 8)
        self.interval = timedelta(minutes=5)
        self.period = 5
        self.term = (self.end_term - 2 * self.period * self.interval, self.end_term)
        self.count_prices = 10
        self.prices = (Decimal('0.000002013014738635'), Decimal('0.000002013014738635'),
                       Decimal('0.000002013014738635'), Decimal('0.000002013014738635'),
                       Decimal('0.000002028930712095'), Decimal('0.000002028930712095'),
                       Decimal('0.000002028930712095'), Decimal('0.00000209'),
                       Decimal('0.00000209'), Decimal('0.000002027452318249'),
                       Decimal('0.000002027452318249'), Decimal('0.000002027452318249'))

        self.typical_prices = (Decimal('0.000002018433779784'), Decimal('0.000002018433779784'),
                               Decimal('0.000002018433779784'), Decimal('0.000002017141261721'),
                               Decimal('0.000002017141261721'), Decimal('0.000002017141261721'),
                               Decimal('0.000002017141261721'), Decimal('0.000002020386218179333333333333333'),
                               Decimal('0.000002023492500437'), Decimal('0.000002023492500437'))

        self.high = Decimal("0.000002023492500437")
        self.low = Decimal("0.000002017141261721")
        self.sma = Decimal("0.000002020330748499066666666666666")
        self.mad = Decimal("2.193045524706666666666667E-9")
        self.cci = Decimal("96.11449473082985314999246847")

        super().__init__(methodName)

    def test_get_price(self):
        prices = self.trd.get_prices(self.mkt, self.term, self.interval)
        self.assertEqual(self.prices, prices)

    def test_get_high(self):
        high = self.trd.get_high(self.mkt, self.term)
        self.assertEqual(self.high, high)

    def test_get_low(self):
        low = self.trd.get_low(self.mkt, self.term)
        self.assertEqual(self.low, low)

    def test_get_typical_prices(self):
        typical_prices = self.trd.get_typical_prices(self.mkt, self.term, self.interval)
        self.assertEqual(self.typical_prices, typical_prices)

        typical_prices = self.trd.get_typical_prices(self.mkt, self.end_term, self.interval, self.count_prices)
        self.assertEqual(self.typical_prices, typical_prices)

    def test_get_simple_moving_average(self):
        sma = self.trd.get_simple_moving_average(market=self.mkt, end_term=self.end_term, interval=self.interval,
                                                 period=self.period)
        self.assertEquals(self.sma, sma)

    def test__get_simple_moving_average(self):
        sma = self.trd._get_simple_moving_average(self.typical_prices, self.period)
        self.assertEquals(self.sma, sma)

    def test_get_mean_absolute_deviation(self):
        mad = self.trd.get_mean_absolute_deviation(market=self.mkt, end_term=self.end_term, interval=self.interval,
                                                   period=self.period)
        self.assertEquals(self.mad, mad)

    def test__get_mean_absolute_deviation(self):
        mad = self.trd._get_mean_absolute_deviation(self.typical_prices, self.period)
        self.assertEquals(self.mad, mad)

    def test_get_cci(self):
        cci = self.trd.get_cci(market=self.mkt, end_term=self.end_term, interval=self.interval,
                               period=self.period)
        self.assertEquals(self.cci, cci)


if __name__ == '__main__':
    unittest.main()
