import logging
from decimal import Decimal
from typing import Tuple, List, Iterator, Iterable

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
from dash.dependencies import Output, Event

from exchanges import Order
from exchanges.objects.glass import Glass


class VisualizerGlassDash:
    _logger = logging.getLogger(__name__)
    
    def __init__(self, glass: Glass, interval: Decimal = None,
                 time_interval: Decimal=0.7*1000):
        self.glass = glass
        try:
            self.interval = Decimal(interval)
            self.time_interval = Decimal(time_interval)
        except TypeError:
            self.interval = None
            self.time_interval = Decimal(0.7 * 1000)

        self.app = dash.Dash(__name__)
        self.colors = {}
    
    def run_server(self):
        self.set_layout()
        
        @self.app.callback(Output('live_market_depth', 'figure'),
                           events=[Event('market_depth_update', 'interval')])
        def update_graph_live():
            self.glass.update_orders()
            
            buy_orders, sell_orders = self.bounded_plot()
            buy_prices, buy_quantity = self.plot_data_former(buy_orders)
            sell_prices, sell_quantity = self.plot_data_former(sell_orders)

            figure = {
                'data':   [
                    {'x': buy_prices, 'y': buy_quantity, 'type': 'line + markers',
                     'name': 'buy orders', 'fill': 'tozeroy'},
                    {'x': sell_prices, 'y': sell_quantity, 'type': 'line + markers',
                     'name': 'sell orders', 'fill': 'tozeroy'}
                ],
                'layout': go.Layout(xaxis=dict(range=[min(buy_prices), max(sell_prices)]),
                                    yaxis=dict(range=[0, max(max(buy_quantity), max(sell_quantity))]))
            }
            return figure
        
        self.app.run_server(debug=True)
    
    @staticmethod
    def plot_data_former(orders: List) -> Tuple[Iterable[Decimal], Iterable[Decimal]]:
        return tuple(Decimal(order.price) for order in orders), tuple(Decimal(order.quantity) for order in orders)

    def set_layout(self):
        self.app.layout = html.Div(
            [
                html.H1(f'''Market depth: 
                        {self.glass.market.market.name}/
                        {self.glass.market.base.name}''',
                        style={'textAlign': 'center'}),
                dcc.Graph(id='live_market_depth', animate=True),
                dcc.Interval(id='market_depth_update', interval=self.time_interval)
            ]
        )
    
    def bounded_plot(self) -> Tuple[List, List]:
        buy_orders, sell_orders = list(self.glass.buy_orders), list(self.glass.sell_orders)
        if self.interval:
            buy_orders = self.bound_list(buy_orders, self.glass.middle_price,
                                         self.interval)
            sell_orders = self.bound_list(sell_orders, self.glass.middle_price,
                                          self.interval)
        return buy_orders, sell_orders
    
    @staticmethod
    def bound_list(orders: List[Order],
                   middle_price: Decimal, interval: Decimal) -> List:
        bounded_list = list(filter(lambda order:
                                   abs(Decimal(order.price) - Decimal(middle_price)) <= \
                                   (interval * Decimal(middle_price))/Decimal(100), orders))
        return bounded_list
