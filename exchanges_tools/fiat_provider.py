from decimal import Decimal
from datetime import timedelta, datetime
from typing import Optional, Iterable, Union, Tuple

from exchanges import ExchangeInterface, Currency, Candle, Order, Market, ExchangeNotSupport


class FiatProvider(ExchangeInterface):
    def __init__(self, _: str = None, __: str = None):
        super().__init__(_, __)
        from currency_converter import CurrencyConverter
        self._api = CurrencyConverter('http://www.ecb.int/stats/eurofxref/eurofxref-hist.zip')

    def is_currencies_present(self, *currencies):
        return len(tuple(filter(lambda c: c not in self.get_currencies(), currencies))) == 0

    def is_currency_traded(self, sold, sale):
        return sold in self.get_currencies() and sale in self.get_currencies()

    def get_price(self, sold: Currency, sale: Currency, date_time: datetime = None) -> Decimal:
        date_time = date_time or datetime.now().date() - timedelta(days=1)
        try:
            return Decimal.from_float(self._api.convert(amount=1.,
                                                        currency=sold.ticker,
                                                        new_currency=sale.ticker,
                                                        date=date_time))
        except ValueError as exp:
            raise ExchangeNotSupport(str(exp))

    def get_currencies(self) -> Iterable[Currency]:
        return Currency.USD, Currency.RUB

    def get_current_price(self, sold: Currency, sale: Currency) -> Decimal:
        return Decimal.from_float(self._api.convert(amount=1.,
                                                    currency=sold.ticker,
                                                    new_currency=sale.ticker))

    def get_balance(self, currency: Currency) -> Decimal:
        return Decimal('0')

    def get_balances(self):
        return {}

    def get_markets(self) -> tuple:
        return Market(Currency.USD, Currency.RUB, '_'),

    def get_deposit_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def get_withdraw_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def get_trading_deals(self, currency: Currency) -> tuple:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def get_order_book(self, currency_first: Currency, currency_second: Currency) -> tuple:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def get_my_open_orders(self, currency: Currency) -> list:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def get_deposit_address(self, currency: Currency) -> str:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def get_all_deposit_addresses(self) -> dict:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def get_indirect_price(self, sold: Currency, sale: Currency, date_time: datetime = None):
        return self.get_price(sold, sale, date_time)

    def buy(self, sold: Currency, sale: Currency, quantity: Decimal, price: Optional[Decimal]) -> Order:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def sell(self, sold: Currency, sale: Currency, quantity: Decimal, price: Optional[Decimal]) -> Order:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def open_order(self, sold: Currency, sale: Currency, price: Decimal, quantity: Decimal, is_buy_type: bool) -> Order:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def cancel_order(self, uid):
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def get_middle_currencies(self, currency_sold, currency_sale) -> tuple:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def withdraw(self, currency: Currency, count: Decimal, address: str) -> str:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def get_candles(self, sold: Currency, sale: Currency, term: Union[Tuple[datetime, datetime], timedelta],
                    interval: timedelta) -> Tuple[Candle]:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")

    def get_valid_intervals(self) -> Optional[Iterable[timedelta]]:
        raise ExchangeNotSupport("`FiatProvider` is detached provider")
