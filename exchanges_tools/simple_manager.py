
from typing import Dict, Union, Optional

from exchanges import CashStorageInterface, Currency, ExchangeInterface
from exchanges.objects.platforms import Platforms
from exchanges.providers.manager import Manager
from exchanges.storage.main_storage import MainStorage

UnionCurrency = Union[Currency, str]

class SimpleManager(Manager):
    def __init__(self, *platforms, user: str = None, storage: Union[CashStorageInterface, bool] = False):
        self._user = user
        if not platforms:
            platforms = tuple(Platforms)

        if storage is True:
            from exchanges import MongoStorage
            storage = MongoStorage.from_configuration()
        else:
            storage = None

        super(SimpleManager, self).__init__({platform: self._user for platform in platforms}, cash_interface=storage)

    def append_provider(self, platform: Union[Platforms, ExchangeInterface], user: str = None):
        if isinstance(platform, ExchangeInterface):
            self._all_providers.append(platform)
            return self

        if platform in self.platforms:
            raise TypeError(f"Can't append provider in SimpleManager. {platform.name} has already been presented.")

        if user != self._user:
            raise ValueError("Can't add provider with other user in this Manager")

        super().append_provider(platform, user)
        return self

    @property
    def _detached_providers(self):
        return self._all_providers

    def get_provider(self, platform: Platforms):
        return self._get_provider(platform)

    def get_percentage(self, estimate_currency: UnionCurrency) -> Dict[Currency, float]:
        if isinstance(estimate_currency, str):
            estimate_currency = Currency.get(estimate_currency)

        balance = self.get_balances()
        prices = {currency: self.get_indirect_price(currency, estimate_currency) for currency in balance.keys()}

        balances = {currency: prices[currency] * count for currency, count in balance.items()}
        bank = sum(balances.values())

        percentage = {currency: balances[currency] / bank for currency in balances.keys()}

        return percentage


def _fire():
    import better_exceptions
    better_exceptions.hook()

    import fire
    fire.Fire(SimpleManager(storage=MainStorage().get_cash_storage()))


if __name__ == '__main__':
    _fire()
