from _pydecimal import Decimal
from datetime import datetime
from typing import Dict, Optional

from exchanges import Currency, Platforms
from exchanges_tools import SimpleManager
from exchanges_tools.simple_manager import UnionCurrency

_mess = "This method don't work in detached manager"


class DetachedManager(SimpleManager):
    def get_balances(self) -> Dict[Currency, Decimal]:
        raise TypeError(_mess)

    def get_estimated_balances(self, est_currency: Currency, date_time: datetime = None):
        raise TypeError(_mess)

    def get_balance(self, currency: Currency) -> Decimal:
        raise TypeError(_mess)

    def get_deposit_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        raise TypeError(_mess)

    def get_withdraw_history(self, currency: Currency, start: datetime, end: datetime) -> tuple:
        raise TypeError(_mess)

    def get_my_open_orders(self, currency: Currency) -> tuple:
        raise TypeError(_mess)

    def get_deposit_address(self, currency: Currency) -> dict:
        raise TypeError(_mess)

    def get_all_deposit_addresses(self) -> dict:
        raise TypeError(_mess)

    def buy(self, sold: Currency, sale: Currency, quantity: Decimal, price: Optional[Decimal]):
        raise TypeError(_mess)

    def sell(self, sold: Currency, sale: Currency, quantity: Decimal, price: Optional[Decimal]):
        raise TypeError(_mess)

    def withdraw(self, currency: Currency, count: Decimal, address: str) -> str:
        raise TypeError(_mess)

    def get_percentage(self, estimate_currency: UnionCurrency) -> Dict[Currency, float]:
        raise TypeError(_mess)
