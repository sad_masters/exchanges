from math import floor
from typing import Optional, Callable, Tuple, Iterable, Collection

from exchanges.providers import ExchangeInterface, Currency, datetime, Decimal, Market, Union, Platforms
from exchanges.services.range_datetime import range_datetime
from datetime import timedelta


class Math:
    @staticmethod
    def get_mean(values: Collection[Decimal]) -> Decimal:
        return sum(values) / len(values)

    @staticmethod
    def get_mean_absolute_deviation(values: Collection[Decimal], func: Callable[[Collection[Decimal]], Decimal],
                                    period: int) -> Decimal:
        def processor(index_value):
            index, value = index_value
            return abs(value - func(values[index - period + 1: index + 1]))

        return sum(map(processor, tuple(enumerate(values))[-period:])) / period


class Trader:
    _cci_coefficient_ = Decimal(200 / 3)
    _default_interval_ = timedelta(minutes=5)  # as example
    _default_count_prices_ = 5  # as example
    _default_period_sma_ = 9
    _default_period_mad_ = 9
    _default_period_cci_ = 14


    def __init__(self, exchange: ExchangeInterface):
        self._exchange = exchange

    def get_prices(self, market: Union[Market, Tuple[Currency, Currency]],
                   term: Union[timedelta, Tuple[datetime, datetime]],
                   interval: Optional[timedelta] = None) -> Tuple[Decimal]:

        if isinstance(term, timedelta):
            term = (datetime.now() - term, datetime.now())

        interval = interval or self._default_interval_

        def get_price(date_time: datetime) -> Decimal:
            return self._exchange.get_price(market[0], market[1], date_time)

        return tuple(map(get_price, range_datetime(term[0], term[1], interval)))

    def get_high(self, market: Union[Market, Tuple[Currency, Currency]],
                 term: Union[timedelta, Tuple[datetime, datetime]]) -> Decimal:
        if isinstance(term, timedelta):
            term = (datetime.now() - term, datetime.now())

        intervals = self._exchange.get_valid_intervals()

        if intervals is None:
            interval = self._default_interval_
        else:
            interval = max(intervals)

        return max(map(lambda candle: candle.high, self._exchange.get_candles(market[0], market[1], term, interval)))

    def get_low(self, market: Union[Market, Tuple[Currency, Currency]],
                 term: Union[timedelta, Tuple[datetime, datetime]]) -> Decimal:
        if isinstance(term, timedelta):
            term = (datetime.now() - term, datetime.now())

        intervals = self._exchange.get_valid_intervals()

        if intervals is None:
            interval = self._default_interval_
        else:
            interval = max(intervals)

        return min(map(lambda candle: candle.high, self._exchange.get_candles(market[0], market[1], term, interval)))

    def get_typical_prices(self, market: Union[Market, Tuple[Currency, Currency]],
                           term: Union[Tuple[datetime, datetime], datetime], interval: Optional[timedelta] = None,
                           count_prices: Optional[int] = 0) -> Tuple[Decimal]:

        interval = interval or self._default_interval_
        count_prices = count_prices or self._default_count_prices_

        if isinstance(term, datetime):
            term = (term - count_prices * interval, term)


        prices: Iterable[Decimal] = \
            ((candle.low + candle.high + candle.close) / Decimal('3')
             for candle in self._exchange.get_candles(market[0], market[1], term, interval or self._default_interval_))

        return tuple(prices)

    def get_simple_moving_average(self,
                                  market: Union[Market, Tuple[Currency, Currency]], end_term: datetime,
                                  interval: timedelta, period: Optional[int] = 0) -> Decimal:
        period = period or self._default_period_sma_
        term = (end_term - 2 * period * interval, end_term)
        typical_prices = self.get_typical_prices(market, term, interval)
        return self._get_simple_moving_average(typical_prices, period)

    def get_mean_absolute_deviation(self,
                                    market: Union[Market, Tuple[Currency, Currency]], end_term: datetime,
                                    interval: timedelta, period: Optional[int] = 0) -> Decimal:

        period = period or self._default_period_mad_
        term = (end_term - 2 * period * interval, end_term)
        typical_prices = self.get_typical_prices(market, term, interval)
        return self._get_mean_absolute_deviation(typical_prices, period)

    def get_cci(self,
                market: Union[Market, Tuple[Currency, Currency]], end_term: datetime, interval: timedelta,
                period: Optional[int] = 0) -> Decimal:
        period = period or self._default_period_cci_

        term = (end_term - 2 * period * interval, end_term)

        typical_prices = self.get_typical_prices(market, term, interval)

        sma = self._get_simple_moving_average(typical_prices, period)
        mad = self._get_mean_absolute_deviation(typical_prices, period)

        return self._cci_coefficient_ * (typical_prices[-1] - sma) / mad

    @staticmethod
    def _get_simple_moving_average(typical_prices: Collection[Decimal], period: Optional[int] = None) -> Decimal:

        period = period or len(typical_prices)

        if period > len(typical_prices):
            raise ValueError

        return Math.get_mean(typical_prices[-period:])

    @staticmethod
    def _get_mean_absolute_deviation(typical_prices: Collection[Decimal], period: Optional[int] = None) -> Decimal:

        period = period or floor(len(typical_prices) / 2)

        if period > floor(len(typical_prices) / 2):
            raise ValueError

        return Math.get_mean_absolute_deviation(typical_prices[-(2 * period - 1):], Math.get_mean, period)


if __name__ == '__main__':
    from simple_manager import SimpleManager

    mng = SimpleManager()
    mng.update_currency()
    trd = Trader(mng.get_provider(Platforms.Idex))
    cci = trd.get_cci(market=(Currency.HOT, Currency.ETH), end_term=datetime(2018, 8, 20, 16, 30, 8),
                      interval=timedelta(minutes=5), period=5)

    high = trd.get_high(market=(Currency.HOT, Currency.ETH), term=(datetime(2018, 8, 20, 16, 30, 8) - 10 * timedelta(minutes=5), datetime(2018, 8, 20, 16, 30, 8)))

    print(high)