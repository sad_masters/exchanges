from datetime import datetime, timedelta
from pathlib import Path
from typing import Tuple, Union, Optional

from exchanges.objects import Currency
from exchanges.providers import ExchangeInterface
from exchanges.services.range_datetime import range_datetime

__increasing__ = '#17BECF'
__decreasing__ = '#7F7F7F'


def _map_candle(argument, candles):
    return tuple(map(lambda candle: getattr(candle, argument), candles))


class Visualizer:
    def __init__(self, exchange: ExchangeInterface, path: Path = Path.cwd()):
        self.exchange = exchange
        self.path = path

    def create_candlestick_plot(self,
                                sold: Currency,
                                sale: Currency,
                                term: Union[timedelta, Tuple[datetime, datetime]],
                                interval: timedelta,
                                plot_file: str = None):

        if isinstance(term, timedelta):
            term = datetime.now() - term, datetime.now()

        candles = self.exchange.get_candles(sold=sold, sale=sale, term=term, interval=interval)

        from pandas import DataFrame
        DataFrame()

        import plotly.graph_objs as go
        trace = go.Candlestick(name=f"{sold.name}{sale.name}",
                               x=_map_candle('date_time', candles),
                               open=_map_candle('open', candles),
                               high=_map_candle('high', candles),
                               low=_map_candle('low', candles),
                               close=_map_candle('close', candles))

        plot_file = self.path / f"{sold.name}_{sale.name}.html" if plot_file is None else plot_file

        import plotly.offline as py
        py.plot([trace], filename=str(plot_file.absolute()))

    def create_price_plot(self, method, term: Union[timedelta, Tuple[datetime, datetime]], interval: timedelta,
                          plot_file: Optional[Path]=None):

        import plotly.offline as py
        import plotly.graph_objs as go

        if isinstance(term, timedelta):
            term = datetime.now() - term, datetime.now()

        values = {dt: method(self.exchange, dt) for dt in range_datetime(term[0], term[1], step=interval)}

        trace = go.Scatter(x=tuple(values.keys()), y=tuple(values.values()))

        plot_file = plot_file \
            if plot_file is not None else self.path / f"price_plot_{int(datetime.now().timestamp())}.html"

        py.plot([trace], filename=str(plot_file.absolute()))
